import { validateMACAddr } from './validate';
const isDate = (s) => {
    if (!s) {
        return false;
    }

    s = s.trim();
    return /\d+-/.test(s);

}

const s = (s1) => {

    const stripSigns = (str) => {
        if (!str) return null;
        if (!isNaN(str)) {
            return str;
        }
        try {

            str = str.toLowerCase();
            str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
            str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
            str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
            str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
            str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
            str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
            str = str.replace(/đ/g, 'd');
        } catch (error) {
            console.log(str)
        }
        return str;
    }

    return {
        equals(s2) {
            return new RegExp(`^${s1}$`, "gmi").test(s2);
        },

        startsWith(s2) {
            return new RegExp(`^${s2}`, "gmi").test(s1);
        },

        hasWord(s2) {
            //if startsWith, return priority 1
            if (new RegExp(`^${s2}`, "gmi").test(s1)) {
                return 1;
            } else if (new RegExp(` ${s2}`, "gmi").test(s1)) {
                return 2;
            }

            let stripped = stripSigns(s1);
            if (!isNaN(stripped)) {
                if (stripped === s2) {
                    return 1;
                }
                return false;
            }
            if (new RegExp(`^${s2}`, "gmi").test(stripped)) {
                return 1;
            } else if (new RegExp(` ${s2}`, "gmi").test(stripped)) {
                return 2;
            }

            return false;
        },
        strip() {
            return stripSigns(s1);
        }
    }
}

const stripSigns = (str) => {
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
    str = str.replace(/đ/g, 'd');
    return str;
}

const convertMac = (macAddr) => {
    if (macAddr && validateMACAddr(macAddr)){
        function upperToHyphenUpper(match, offset, string) {
            return match.toUpperCase();
        }
        macAddr = macAddr.replace(/[a-f]/g, upperToHyphenUpper);
        return macAddr;
    }
    return macAddr;
}

export default {
    isDate,
    stripSigns,
    s,
    convertMac
}