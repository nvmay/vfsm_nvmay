
const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

const daydiff = (date1, date2) => {
    let dt1 = new Date(date1);
    let dt2 = new Date(date2);
    return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24));
}

const formatTime = (date) => {
    if (date === null || date === undefined || date === '') {
        return null;
    }
    const convert = new Date(date);
    let h = convert.getHours();
    let mi = convert.getMinutes();
    let se = convert.getSeconds();
    if (mi < 10) {
        mi = '0' + mi;
    }
    if (h < 10) {
        h = '0' + h;
    }
    if (se < 10) {
        se = '0' + se;
    }

    return monthNames[convert.getMonth()] + " " + convert.getDate() + ", " + convert.getFullYear() + " " + h + ":" + mi + ":" + se;
}

const formatTimeDetail = (date) => {
    if (date === null || date === undefined || date === '') {
        return null;
    }
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    const convert = new Date(date);
    let h = convert.getHours();
    let mi = convert.getMinutes();
    let se = convert.getSeconds();
    if (mi < 10) {
        mi = '0' + mi;
    }
    if (h < 10) {
        h = '0' + h;
    }
    if (se < 10) {
        se = '0' + se;
    }
    
    return monthNames[convert.getMonth()] + " " + convert.getDate() + ", " + convert.getFullYear() + "  -  " + h + ":" + mi + ":" + se;
}

const formatTimeToSend = (dateToFormat) => {
    if (dateToFormat === null || dateToFormat === undefined || dateToFormat === '' ) {
        return null;
    }
    const convert = new Date(dateToFormat);
    let month = convert.getMonth();
    let date = convert.getDate();
    if (month < 9) {
        month = '0' + (month + 1);
    } else {
        month = month + 1;
    }
    if (date < 10) {
        date = '0' + date;
    }
    return convert.getFullYear() + '-' + month + '-' + date;
}

export default {
    daydiff,
    formatTime,
    formatTimeToSend,
    formatTimeDetail
}