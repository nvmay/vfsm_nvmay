export const validateFiles = (file, arrAllowFiles) => {
    const regex = new RegExp(`([a-zA-Z0-9\s_\\.\-:\(\)])+(${arrAllowFiles.join('|')})$`);
    if (regex.test(file.name.toLowerCase())) {
        return true;
    }
    return false;
};

export const validateEmail = (email) => {
    const regex = /[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;

    if (email && regex.test(email.toLowerCase())) {
        return true;
    }
    return false;
};

export const validatePWLowerCase = (password) => {
    const regex = /[a-z]/;
    if (password && regex.test(password)) {
        return true;
    }
    return false;
}

export const validatePWUpperCase = (password) => {
    const regex = /[A-Z]/;
    if (password && regex.test(password)) {
        return true;
    }
    return false;
}

export const validatePWDigit = (password) => {
    const regex = /[0-9]/;
    if (password && regex.test(password)) {
        return true;
    }
    return false;
}

export const validatePWAlpha = (password) => {
    const regex = /[!@#$%^&*]/;
    if (password && regex.test(password)) {
        return true;
    }
    return false;
}

export const validateMACAddr = (mac) => {
    var regex = /^([0-9a-fA-F][0-9a-fA-F]:){5}([0-9a-fA-F][0-9a-fA-F])$/;

    if (mac && regex.test(mac)) {
        return true;
    }
    return false;
} 

export const checkInputExit = (input) => {
    if( input !== undefined && input !== '' && input != null){
        return true;
    }
    return false;
}

export const compareTwoInputs = (str1, str2) => {
    if(checkInputExit(str1) && checkInputExit(str2)){
        return (str1 === str2);
    }
    return false;
}