
import React, { Component } from 'react';
import Node from './Node';
import { DragDropContext } from 'react-dnd'
import HTML5Backend from 'react-dnd-html5-backend'

@DragDropContext(HTML5Backend)
class Tree extends Component {

    render() {
        const { data, renderNodeName } = this.props;

        return (
            <div className='tree'>
                <Node data={data} level={0} renderNodeName={renderNodeName}/>
            </div>
        )
    }
}

export default Tree;