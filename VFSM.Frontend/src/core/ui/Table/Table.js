
import React, { Component } from 'react';
import { Table as AntdTable } from 'antd';

class Table extends Component {
    render() {
        const { columns, data, ...otherProps } = this.props;
        return (
            <AntdTable columns={columns} dataSource={data} {...otherProps} />
        );
    }
};

export default Table;
