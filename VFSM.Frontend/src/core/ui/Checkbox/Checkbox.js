import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Checkbox as AntCheckbox } from 'antd';
import './style.scss';

export default class Checkbox extends Component {

    componentDidMount(){
        const node = ReactDOM.findDOMNode(this);
        const inner = node.querySelector('.ant-checkbox-inner');
        inner.className = 'icon-check';

    }

    render() {
        const { className, ...otherProps } = this.props;
        return( 
            <AntCheckbox className={'custom-checkbox ' + (className || '')} {...otherProps}>{this.props.children}</AntCheckbox>
        )
    }
}