import React, { Component } from 'react';
import './style.scss';
import { Select } from 'antd';
import ApiService from '../../services/ApiService';

const Option = Select.Option;
const api = new ApiService();

class SelectBox extends Component {
    state = {
        focused: false,
        data: null
    }

    componentDidMount() {
        const { service, params } = this.props;
        const METHOD = 'post';
        const url = service;

        api.callApi(METHOD, url, params).then(response => {

            this.setState({
                data: response.response
            })
        })
    }

    onFocus = e => {
        this.setState({ focused: true })
    }

    onBlur = e => {
        this.setState({ focused: false })
    }


    render() {
        const { className, labelKey, valueKey, ...otherProps } = this.props;

        const { data } = this.state;
        const options = data ? data.map((item, index) => {
            return (
                <Option value={item[valueKey]} key={index}>{item[labelKey]}</Option>
            )
        }) : null;
        return (

            <Select
                className={'custom-select ' + (className || '')}
                showSearch
                allowClear
                style={{ width: 200 }}
                optionFilterProp="children"
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                placeholder={<div><span>Type:</span> Search type</div>}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                {...otherProps}
            >
                {options}
            </Select>
        );
    }

};

export default SelectBox;
