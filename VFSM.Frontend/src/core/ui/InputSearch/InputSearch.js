import React, { Component } from 'react';
import Icon from '../Icon';

class InputSearch extends Component {
    render() {
        const { ...otherProps } = this.props;
        return (
            <Input placeholder="Select Search Type" prefix={<Icon type='search'/>}  {...otherProps} />

        );
    }

};

export default InputSearch;
