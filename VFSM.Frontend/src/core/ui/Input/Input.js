
import React, { Component } from 'react';
import { Input as AntdInput } from 'antd';
import Icon from '../Icon/Icon';

class Input extends Component {
    render() {
        const { icon, inputRef, ...otherProps } = this.props;
        return (
            <AntdInput prefix={<Icon type={icon} />}  ref = {inputRef}  {...otherProps} />

        );
    }
};

export default Input;
