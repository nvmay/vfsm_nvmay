import { observable, action, configure, runInAction } from 'mobx';
import { DialogService } from '../../core/services'
import ApiStore from './ApiStore';
import NProgress from 'nprogress/nprogress.js';
import 'nprogress/nprogress.css';


// don't allow state modifications outside actions
configure({
    enforceActions: 'observed'
});
export default class BaseStore {

    constructor(rootStore) {
        this.rootStore = rootStore;
    }

    call(jobRequest, handleResponse, handleError) {
        const Settings = {
            template: '<div id="nprogress-container"><div class="bar" role="bar"><div class="peg"></div></div><div class="spinner" role="spinner"><div class="spinner-icon"></div></div></div>'
        }
        NProgress.configure(Settings);
        NProgress.start();
        ApiStore.setLoading(true);
        jobRequest().then(response => {
            runInAction(() => {
                ApiStore.setLoading(false);
            })
            NProgress.done();
            // DialogService.showLoading(false); // Is loading success
            if (response.response && handleResponse) {
                runInAction(() => {
                    handleResponse(response);
                });
            }
            if (response.error) {
                if (handleError) {
                    handleError(response.error);
                    return;
                }
            }
        }).catch(error => {
            if (handleError) {
                handleError(error);
                return;
            }
            runInAction(() => {
                // DialogService.showLoading(true); // Is loading data
                console.log(`Error happen:${error}`);
            });
        })

    }

    // async requestAction(Action, jobRequest, handleResponse, handleError) {
    //     const {
    //         GlobalUI
    //     } = this.rootStore;
    //     GlobalUI.loadingPages(true); // Is loading data
    //     try {
    //         const response = await jobRequest();
    //         GlobalUI.loadingPages(true); // Is loading success
    //         runInAction(() => {
    //             handleResponse(response).bind(this);
    //         });
    //     } catch (error) {
    //         if (handleError) {
    //             handleError().bind(error);
    //             return;
    //         }
    //         runInAction(() => {
    //             GlobalUI.loadingPages(true); // Is loading data
    //             console.log(`Error happen:${error}`);
    //         });
    //     }
    // }
}