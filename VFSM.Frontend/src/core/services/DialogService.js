

import { Modal } from 'antd';

let currentDialog;

export default {

    show: (component, options) => {
        currentDialog = Modal.info({
            className:'custom-modal',
			content: component,
            ...options
        })
    },

    close: ()=> {
        if(currentDialog){
            currentDialog.destroy();
        }
    },

    showLoading: (flag) => {
        //CODE: 
    }

}

