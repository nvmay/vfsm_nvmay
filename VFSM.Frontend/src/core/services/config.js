const ENV_MODE = 'prod'; // prod|test|dev1|dev2
export const APP_VERSION = 'VF (0.8)';

const URL_CONFIG = {
    prod: {
        domain: '13.229.115.178',
        port: '8051',
        ssl: false
    },
    test: {
        domain: '18.220.146.229',
        port: '9091',
        ssl: false
    },
    dev_payroll: {
        domain: '192.168.100.206',
        port: '8084',
        ssl: false
    },
    dev_hr: {
        domain: '192.168.100.206',
        port: '8086',
        ssl: false
    },
    dev: {
        domain: 'localhost',
        port: '53234',
        ssl: false
    }
}

export default URL_CONFIG[ENV_MODE];