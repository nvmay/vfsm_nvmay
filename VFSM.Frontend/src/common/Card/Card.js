import React from 'react';
import {Card, Modal, Avatar } from 'antd';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import IconSvg from 'common/icon/IconSvg';
import SmallSelect from 'common/SmallSelect/SmallSelect';
import ConfirmModal from 'common/ConfirmModal/ConfirmModal';

import './cardstyle.scss'
@inject('rootStore')
@observer
class ModelCard extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            isOpenDetail: false,
            isOpenConfirm: false,
            isConfirmDisable: false,
            idDelete: undefined,
            isLoading: false
        }
    }

    /* handle click menu */
    handleClickMenuCallBack =( eventName) => {
        const { Id } = this.props;
        if(eventName === 'Detail'){
            this.setState({
                isOpenDetail: true
            });
            this.props.handleClickOpenDetail(Id, '');
        } else if (eventName === 'Remove') {
            // Delete 
            this.props.handleClickDeleteCard(this.props.Id);
        }
    }

    /* */
    handleDeleteModel = (id) => {
        //this.props.rootStore.model.deleteModel(id);
        this.setState({
            isOpenConfirm: true,
            idDelete: id
        })
    }

    handleCloseConfirmModal = () => {
        this.setState({
            isOpenConfirm: false,
            idDelete: undefined,
            isLoading: false
        })
    }

    deleteModelCallBack = () =>{
        const deleteDetails = this.props.rootStore.model.details ? this.props.rootStore.model.details : {};
        if(deleteDetails) {
            if ( deleteDetails.status.toUpperCase() === 'SUCCESS') {
                this.setState({
                    isOpenConfirm:false,
                    isLoading: false
                })
            }
        } else {
            this.setState({
                isConfirmDisable: false,
                isLoading: false
            })
        }
     
    }

    handleConfirm = () => {
        this.props.rootStore.model.deleteModel(this.state.idDelete , this.deleteModelCallBack);
        this.setState({
            isConfirmDisable: true,
            isLoading: true
        })
    }

    /* */
    handleCloseModelModal = () => {
        this.setState({
            isOpenDetail: false
        });
    }

    handleClickCancelCallback =() => {
        this.setState({
            isOpenDetail: false
        });
    }

    handleClickOpenDetail = (id, name) => {
        this.props.handleClickOpenDetail(id, name);
    }

    render() {
        const { isOpenConfirm, isConfirmDisable, isLoading } = this.state;
        const {data, Id, avatar, selectOption, menuRole, exception} = this.props;
        return (
            <div style={{'position': 'relative'}}>
                <Card 
                    className="model-card-detail" 
                    style={{
                        marginTop: '20px',
                        borderRadius: '2px',
                        width:'100%',
                        backgroundColor:'#F4F4F4'}}
                    onClick={ () => this.handleClickOpenDetail(Id, data.name)}
                >
                
                    { avatar ? <Avatar size={69} shape="square" src={avatar} /> : <div className="avatar-null"></div> }
                    <div className="model-card-container" >
                        {data.name}
                    </div>
                </Card>
                <div className="model-card-icon-wapper">
                    {
                        menuRole&& menuRole == 'operation.write'? ( !exception ? <SmallSelect  selectOption={selectOption} handleClickMenuCallBack={this.handleClickMenuCallBack}/> : null ) : null
                    }
                </div>
                <ConfirmModal  
                    visible={isOpenConfirm}
                    confirmDisable={isConfirmDisable}
                    title={"Do you want to delete ?"} 
                    handleCloseModal={this.handleCloseConfirmModal} 
                    handleConfirm={this.handleConfirm}
                    loading={isLoading}
                />
            </div>
        );
    }
}

ModelCard.PropTypes = {
    data: PropTypes.object.isRequired,
    modelId: PropTypes.string.isRequired
}

export default ModelCard;