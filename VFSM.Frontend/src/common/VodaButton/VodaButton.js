import React from 'react';
import { Button } from 'antd';
import PropTypes from 'prop-types'
import IconSvg from '../icon/IconSvg';
import './styles.scss'

class VodaButton extends React.Component {

    handleClick = () => {
        this.props.handleClickButton();
    }
    render() {
        const { iconType, text, ...otherProps } = this.props;
        return (
            <Button className="btn-general ml-3" { ...otherProps }>
                <IconSvg name={iconType} />
                <span>{text}</span>
            </Button>);
    }
}

VodaButton.propTypes = {
    iconType: PropTypes.string,
    text: PropTypes.string.isRequired
}
export default VodaButton;