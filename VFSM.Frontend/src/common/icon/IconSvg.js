import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactSVG from 'react-svg';
import Admin from './svg/ADMIN@2x.svg';
import Arrow from './svg/ARROW@2x.svg';
import DatePicker from './svg/Calendar@2x.svg'
import Dashboard from './svg/DASHBOARD.svg';
import DeviceEvent from './svg/DEVICE EVENT@2x.svg';
import ExportExcel from './svg/EXPORT EXEL@2x.svg';
import Maintenance from './svg/MAINTENANCE@2x.svg';
import Monitoring from './svg/MONITORING@2x.svg';
import Report from './svg/REPORT@2x.svg';
import Search from './svg/SEARCH@2x.svg';
import More from './svg/SEE MORE@2x.svg';
import Plus from './svg/Plus icon@2x.svg';
import Pin0 from './svg/Pin 0@2x.svg';
import Pin1 from './svg/Pin 1@2x.svg';
import Pin2 from './svg/Pin 2@2x.svg';
import Pin3 from './svg/Pin 3@2x.svg';
import Pin4 from './svg/Pin 4@2x.svg';
import Location from './svg/Location@2x.svg';

const ICONS = {
    Admin,
    Arrow,
    DatePicker,
    Dashboard,
    DeviceEvent,
    ExportExcel,
    Maintenance,
    Monitoring,
    Report,
    Search,
    More,
    Plus,
    Pin0,
    Pin1,
    Pin2,
    Pin3,
    Pin4,
    Location
};
class IconSvg extends Component {
    render() {
        const { name, ...otherProps } = this.props;
        const path = ICONS[name];
        return (
            <ReactSVG path={path}  {...otherProps} />

        );
    }

};

IconSvg.propTypes = {
    name: PropTypes.string
};

export default IconSvg;
