
import React from 'react';
import 'antd/dist/antd.css';
import './styles.scss';
import { Table } from 'antd';

class VodaTable extends React.Component {
    constructor (props) {
        super(props);
    }

    render () {
        const { columns, data, ...otherProps } = this.props;
        return (
            <Table columns={columns} dataSource={data}  {...otherProps } pagination={false} />
        );
    }
}


export default VodaTable;
