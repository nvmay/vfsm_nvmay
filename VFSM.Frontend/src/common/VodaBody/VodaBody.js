import React from 'react';
import { Pagination } from 'antd';
import Select from '../Select/Select';
import './styles.scss'
import IconSvg from '../icon/IconSvg';

class VodaBody extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            disableOrderSort: true,
            activeAsc: '',
            activeDes: ''
        }
    }

    handleSort = (value) => {
        this.props.onChangeSort(value);
        if (!value) {
            this.setState({ disableOrderSort: true });
        } else {
            this.setState({ disableOrderSort: false });
        }
    }

    changePage = (pageNumber) => {
        this.props.onChangePage(pageNumber);
    }

    handleSortOrder = (type) => {
        this.props.onSortOrder(type);
        if (type === true) {
            this.setState({
                activeAsc: 'active',
                activeDes: ''
            })
        } else {
            this.setState({
                activeAsc: '',
                activeDes: 'active'
            })
        }
    }

    render() {
        const { ...otherProps } = this.props;
        return (
            <div className={otherProps.className}>
                <div className="monitoring-sort">
                    {
                        otherProps.optionSort&&
                        <Select className="ml-1" placeholder="Sort by" option={otherProps.optionSort} onChange={this.handleSort} />
                    }
                    <div className="sort-order">
                        {
                            otherProps.optionSort&&
                            (
                            !this.state.disableOrderSort
                                ? <div>
                                    <div onClick={() => this.handleSortOrder(true)} className={"ascending " + this.state.activeAsc}>
                                        <IconSvg name="Arrow" />
                                    </div>
                                    <div onClick={() => this.handleSortOrder(false)} className={"descending " + this.state.activeDes}>
                                        <IconSvg name="Arrow" />
                                    </div>
                                </div>
                                : <div>
                                    <div className="ascending">
                                        <IconSvg name="Arrow" />
                                    </div>
                                    <div className="descending">
                                        <IconSvg name="Arrow" />
                                    </div>
                                </div>
                            )
                        }
                    </div>
                </div>
                <div className="monitoring-data">
                    {otherProps.body}
                </div>
                {
                    otherProps.pageCount > 1 &&
                    <div className="monitoring-paging">
                        <Pagination showQuickJumper defaultCurrent={1} total={otherProps.pageCount * 10} onChange={this.changePage} />
                    </div>
                }
            </div>
        );
    }
}

VodaBody.propTypes = {
}
export default VodaBody;