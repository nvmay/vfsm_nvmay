import React from 'react';
import { Button } from 'antd';
import PropTypes from 'prop-types';
import './styles.scss';

class ButtonFooter extends React.Component {

    handleClickFirst = () => {
        this.props.handleClickFirst();
    }

    handleClickCancel = () => {
        this.props.handleClickCancel();
    }

    render() {
        const { text, disabled, className, loading, ...otherProps } = this.props;
        return (
            <div className={"button-footer " + className}>
                <Button className="btn-general ml-3" text={text} disabled={disabled} onClick={this.handleClickFirst} loading={loading} {...otherProps} >
                    <span>{text}</span>
                </Button>
                <Button className="btn-cancel ml-3" onClick={this.handleClickCancel}>
                    <span>Cancel</span>
                </Button>
            </div>
        );
    }
}

ButtonFooter.propTypes = {
    text: PropTypes.string.isRequired,
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    handleClickFirst: PropTypes.func.isRequired,
    handleClickCancel: PropTypes.func.isRequired
}
export default ButtonFooter;