import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Cookies from 'js-cookie';
import { toJS } from "mobx";
import { inject, observer } from 'mobx-react';
import './style.scss'
import { Avatar, Row, Col} from 'antd';
import VodaButton from '../VodaButton/VodaButton';
// import routes from '../../routes';
import CustomSelect from '../CustomSelect/CustomSelect';

const MENU_EVENT = {
    NOTIFICATION: 'Notification',
    SETTING: 'Account Setting',
    LOGOUT: 'Log Out',
    HELP: 'Help'
}
@inject('rootStore')
@inject('menuRoleStore')
@observer
class Header extends Component {
    constructor (props){
        super(props);
        this.token = Cookies.get('token');
    }

    componentDidMount () {
        // if (this.token && this.token.length > 0) {
        //     if (window.location.href === window.location.origin
        //         || window.location.href === `${window.location.origin}/`) {
        //         window.location.replace('/main');
        //     } else {
        //        // window.location.replace({pathname: '/main', query: {return_to: window.location.href}});
        //     }
        // } else {
        //     window.location.replace('/login');
        // }
    }

    switchLocation = () => {
        const { locationName } = this.props;
        if (locationName === '/main/Maintenance/OTA') {
            return 'OTA';
        }
        const { routes } = this.props.menuRoleStore;
        let objRoute = routes.find(o => o.path === locationName && o.operationValue !== '');
        if (!objRoute) {
            for (let route of routes) {
                if (route.operationValue !== '') {
                    if (route.children) {
                        for (let child of route.children) {
                            if (child.path === locationName) {
                                objRoute = child;
                            }
                        }
                    }
                    if (route.childrenPage) {
                        for (let child of route.childrenPage) {
                            if (child.path === locationName) {
                                objRoute = child;
                            }
                        }
                    }
                }
            }
        }
        let pageName = objRoute ? (objRoute.title ? objRoute.title : objRoute.name ): 'Welcome to V-SOS system.';
        return pageName;
    };

    handleClickMenuCallBack = (eventName) => {
        if(eventName){
            switch (eventName) {
                case MENU_EVENT.NOTIFICATION : 
                    break;
                case MENU_EVENT.SETTING:
                    break;
                case MENU_EVENT.LOGOUT:
                    this.onLogOut();
                    break;
                case MENU_EVENT.HELP:
                    break; 
            }
        }
    }

    onLogOut = () => {
       Cookies.remove('token', { path: '/'});
       window.location.replace('/login');
    }

    handleExportMonitoring = () => {
        this.props.rootStore.user.exportUser();
    }

    render() {
        const name = this.switchLocation();
        const { menuRoles } = this.props.menuRoleStore;
        const { users } = this.props.rootStore.user;
        const selectOption = [
            // {
            //     "title" : MENU_EVENT.NOTIFICATION,
            //     'icon': 'Report',
            //     disabled: false,
            //     "className" : "select-option",
            //     'content': MENU_EVENT.NOTIFICATION,
            // },
            // {
            //     "title" : MENU_EVENT.SETTING,
            //     'icon': 'Maintenance',
            //     disabled: false,
            //     "className" : "select-option",
            //     'content':MENU_EVENT.SETTING,
            // },
            {
                "title" : MENU_EVENT.LOGOUT,
                'icon': 'Admin',
                disabled: false,
                "className" : "select-option",
                'content': MENU_EVENT.LOGOUT
            },
            // {
            //     "title" : MENU_EVENT.HELP,
            //     'icon': 'DeviceEvent',
            //     disabled: true,
            //     "className" : "select-option",
            //     'content':  MENU_EVENT.HELP,
            // },
        ]

        return (
            <div className="header row">
                <div className="inner-left column flex">
                    <div className="row-between full-height">
                        <h1 className="title full-height row ml-6">{name}</h1>
                        { menuRoles 
                        && menuRoles.Monitoring !== '' 
                        && name === 'Monitoring' 
                        && <VodaButton  disabled={users&& users.items&&users.items.length > 0 ? false: true} 
                                        text="Export Excel"     
                                        iconType="ExportExcel" 
                                        onClick={this.handleExportMonitoring}/>}
                    </div>
                </div>
                <div className="user-info-wrapper">
                    <Row className='vfsm-info-content'>
                        <Col span={8} className="vfsm-user-avatar">
                            <CustomSelect selectOption={selectOption}
                                handleClickMenuCallBack={this.handleClickMenuCallBack}
                                sharp={<Avatar size={54} icon="user"/>}
                            />
                        </Col>
                        <Col className="vfsm-user-name-wrapper">
                            <span className="user-name">Humax Vina</span>
                        </Col>
                    </Row>
                </div>
            </div>
        );
    }

};

Header.propTypes = {
    locationName: PropTypes.string.isRequired,
};
export default Header;
