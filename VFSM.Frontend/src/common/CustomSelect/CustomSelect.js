import React from 'react';
import { Menu, Dropdown, Row , Col } from 'antd';
import IconSvg from '../icon/IconSvg';
import PropTypes from 'prop-types';
import './styles.scss'

export default class CustomSelect extends React.Component {
    constructor(props){
        super(props);
    }

    handleClickMenu = (eventName) => {
        this.props.handleClickMenuCallBack(eventName);
    }

    menu = () => {
        const {selectOption, } = this.props;
        return <Menu style={{ width: 165 }}>
            {
                selectOption && selectOption.map((prop, index) =>
                    <Menu.Item key={index} className={prop.className ? prop.className : ''} disabled={prop.disabled ? prop.disabled : false}>
                        {
                            <Row >
                                <Col span={5}>
                                    {
                                        prop.icon && 
                                        <IconSvg name={prop.icon} className='vfsm-custom-select-icon'/>
                                    }
                                </Col>
                                <Col>
                                    {
                                        prop.disabled
                                        ? <span>{prop.title ? prop.title : ''}</span>
                                        : 
                                        (
                                            prop.content ?
                                            <div className={prop.className ? prop.className : ''} onClick={() => this.handleClickMenu(prop.title)}>
                                                {prop.content ? prop.content : ''}
                                            </div>
                                            : 
                                            <span>{prop.title ? prop.title : ''}</span>
                                        )
                                    }
                                </Col>
                            </Row>
                        }
                    </Menu.Item>
                )
            }
        </Menu>
    };
    render() {
        const {sharp, ...otherProps} = this.props;
        return (
            <div className="custom-select-wapper">
                <Dropdown overlay={this.menu()} trigger={['click']} >
                    <div>
                        {sharp}
                    </div>
                </Dropdown>
            </div>
        )
    }

}

CustomSelect.propTypes = {
    sharp: PropTypes.string.isRequired,
    title: PropTypes.string,
}
