import React, { Component } from 'react';
import './style.scss';
import { Select } from 'antd';

const Option = Select.Option;

class SelectBox extends Component {
    state = {
        focused : false
    }

    onFocus = e => {
        this.setState({focused : true})
    }

    onBlur = e => {
        this.setState({focused: false})
    }


    render() {
        const { className, label, options, ...otherProps } = this.props;
        // const { focused } = this.state;
        if(options){
            return (
                <div>
                    <Select
                        className ={'custom-select ' + (className || '')}
                        disabled={this.props.disabled}
                        defaultValue={this.props.defaultValue}
                        showSearch
                        allowClear
                        style={{ width: 200 }}
                        optionFilterProp="children"
                        onFocus={this.onFocus}
                        onBlur={this.onBlur}
                        placeholder = {this.props.placeholder}
                        filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                        {...otherProps}
                    >
                    {
                        this.props.option.map( item => {
                            return (<Option value={item.value} key={item.value}>{item.label}</Option>);
                        })
                    }
                    </Select>
                    { options &&
                        <div className="vfsm-select-msg-wrapper">
                            {
                                options.isShowError &&
                                <div className="vfsm-select-error">
                                    {options.errorMsg ? options.errorMsg : ''}
                                </div>
                            }
                            {
                                options.isShowGuidMsg && !options.isShowError &&
                                <div className="vfsm-select-guimsg">
                                    {options.errorMsg ? options.errorMsg : ''}
                                </div>
                            }
                        </div>
                    }
                </div>
            );
        } else {
            return (
            <Select
                className ={'custom-select ' + (className || '')}
                disabled={this.props.disabled}
                defaultValue={this.props.defaultValue}
                showSearch
                allowClear
                style={{ width: 200 }}
                optionFilterProp="children"
                onFocus={this.onFocus}
                onBlur={this.onBlur}
                placeholder = {this.props.placeholder}
                filterOption={(input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                {...otherProps}
            >
            {
                this.props.option.map( item => {
                    return (<Option value={item.value} key={item.value}>{item.label}</Option>);
                })
            }
            </Select>
        )}
    }

};

export default SelectBox;
