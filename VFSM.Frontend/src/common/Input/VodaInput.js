import React from 'react';
import { Input } from 'antd';
import PropTypes from 'prop-types';

import './styles.scss';

class VodaInput extends React.Component{

    constructor(props){
        super(props);
        this.state={
          
        }
    }

    render(){
        const { title, disabled, options, width , height , ...otherProps} = this.props;
        const inputStyle = {
            width: `${width}%`,
            borderColor: options.isShowError ? 'red' : '#D4DAE7',
        }

        const inputStyleTitle = {
            width: `${width}%`,
            height: height ? height : '30px',
            border: '1px solid #C4C4C4',
            backgroundColor: options.isShowError ? '#FBFEC4' : '#FFFFFF',
        }

        const length = {
            maxLength: 100,
            minLength: 6,
        }
        return(

            <div className={title ? "vfsm-voda-input-with-title-wrapper" : "vfsm-voda-input-wrapper"}>
                <div className="vfsm-voda-input-title-wrapper">
                    {
                        title && <div className="vfsm-voda-input-title">
                        {title}
                    </div>
                    }
                    {
                        title && options.isShowError && 
                        <div className="vfsm-input-error-msg-title">
                            {options.errorMsg ? options.errorMsg : ''}
                        </div>
                    }
                    {
                        title && options.isShowGuidMsg && 
                        <div className="vfsm-input-guid-msg-title">
                            {options.guidMsg ? options.guidMsg : ''}
                        </div> 
                    }
                </div>
                <Input 
                    style={ title ? inputStyleTitle : inputStyle}
                    {...otherProps}
                    disabled={disabled}
                    maxLength={options.maxLength ? options.maxLength : length.maxLength}
                    minLength={options.minLength ? options.minLength : length.minLength}
                />
                {
                (options.isShowError || options.isShowGuidMsg) &&
                 <div className="vfsm-input-error-box">
                    {
                        options.isShowError && !title &&
                        <div className="vfsm-input-error-msg">
                            {options.errorMsg ? options.errorMsg : ''}
                        </div>
                    }
                    {
                        options.isShowGuidMsg && !options.isShowError &&  !title &&
                        <div className="vfsm-input-guid-msg">
                            {options.guidMsg ? options.guidMsg : ''}
                        </div>
                    }
                </div>
                }
            </div>
        )
    }

}

VodaInput.PropTypes = {
  //  title: PropTypes.string.isRequired,
   options: PropTypes.object.isRequired,
    disabled : PropTypes.bool.isRequired,
    width: PropTypes.number.isRequired,
}

export default VodaInput;