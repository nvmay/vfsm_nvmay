import React from 'react'; 
import { Checkbox } from 'antd';

import './styles.scss'

export default class VodaCheckBox extends React.Component{

    constructor(props){
        super(props);
    }

    render(){
         const { text, ...otherProp } = this.props;
        return(
           <div className="vfsm-checkbox-wrapper">
                <Checkbox  {...otherProp }>{text ? text : ''}</Checkbox>
           </div>
        )
    }

}