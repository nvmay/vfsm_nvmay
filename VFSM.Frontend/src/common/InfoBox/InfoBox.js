import  React from 'react'
import ColModal from '../ColModal/ColModal';

export default class InfoBox extends React.Component {
  
    constructor(props){
        super(props);
    }

    render() {

    const {data } = this.props;

    return (
      <div>
        {
            data.map( (prop, index ) =>
                <div className= "model-detail-content-wapper">
                    <ColModal data={prop} key={index} />
                    {
                         prop.hasFooter && <div className ='model-detai-footer'></div>
                    }
                </div>
            ) 
        }
      </div>
    )
  }
}
