/**
 * Upload file return base64
 */

'use strict';

import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import './styles.scss'

class UploadFile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            files: [],
            label: ([<label for='file' className='chooseFile'>Choose a file</label>])
        };
        this.handleChange = this.handleChange.bind(this);
    }

    //
    handelUpload = (files) => {
        const { multiple, onDone } = this.props;
        const allFiles = [];
        for (let i = 0; i < files.length; i++) {
            const file = files[i];

            // Make new FileReader
            const reader = new FileReader();

            // Convert the file to base64
            reader.readAsDataURL(file);

            // on reader load something
            reader.onload = () => {
                // Make a info object file
                const fileInfo = {
                    name: file.name,
                    type: file.type,
                    size: Math.round(file.size / 100),
                    fileBase64: reader.result.replace('data:application/octet-stream;base64,', ''),
                    file
                };

                // Push it to the state
                allFiles.push(fileInfo);
                // If all files have been proceed
                if (allFiles.length === files.length) {
                    // Callback function
                    if (multiple) {
                        onDone(allFiles);
                    } else {
                        onDone(allFiles[0]);
                    }
                }
            };
        }
    };

    handleChange = (e) => {
        const { onBeforeUpload } = this.props;
        let labelItem = [];
        // Get files
        const files = e.target.files;
        if(files.length > 0) {
            for (let i = 0; i < files.length; i++) {
                const file = files[i];
                const text = file.name.length > 35 ? file.name.substring(0, 35): file.name;
                labelItem.push(<label for='file' className='showFile'>{ text }</label>);
                labelItem.push(<br/>);
            }
            this.setState({
                label: labelItem
            })
        }
        if (onBeforeUpload) {
            if (onBeforeUpload(files)) {
                this.handelUpload(files);
            } else {
                //
            }
        } else {
            this.handelUpload(files);
        }

        // Reset file input
        // e.target.value = "";
    }
    handleClick = (e) => {
        e.target.value = null;
    }

    render() {
        const { multiple, accept, isUploading } = this.props;
        const { label } = this.state;
        return (<Fragment>
                    { label }
                    <input
                        className='inputFile'
                        id='file'
                        name='file'
                        type='file'
                        multiple={ multiple }
                        accept={ accept }
                        onClick={ this.handleClick }
                        onChange={ e => this.handleChange(e) } />
                    {isUploading && <span style={ { paddingLeft: '15px', fontWeight: '600' } }>Uploading...</span>}
                </Fragment>
        );
    }
}

UploadFile.propTypes = {
    multiple: PropTypes.bool,
    accept: PropTypes.string,
    isUploading: PropTypes.bool,
    onDone: PropTypes.func.isRequired,
    onBeforeUpload: PropTypes.func
};

UploadFile.defaultProps = {
    multiple: true,
    isUploading: false,
    accept: '',
    onBeforeUpload: () => {}
};

export default UploadFile;
