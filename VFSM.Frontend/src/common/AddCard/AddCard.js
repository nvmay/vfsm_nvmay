import React from 'react';

import IconSvg from '../../../src/common/icon/IconSvg';

import './styles.scss';

export default class AddCard extends React.Component{
    
    constructor(props){
        super(props);
    }

    componentDidMount(){
        
    }

    render(){
        const { handleClick } = this.props;
        return(
            <div className='add-card-model-wapper' onClick={handleClick}>
                <div className="add-card-model-circle">
                    <IconSvg name="Plus" className="add-card-icon"/>    
                </div>
            </div>
        )
    }
}