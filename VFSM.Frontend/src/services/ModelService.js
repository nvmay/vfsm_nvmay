'use strict';

import { ApiService } from '../core/services';

class ModelService extends ApiService {

    constructor(){
        super('model');
    }

    getAll(body) {
		return this.get('search', body);
	}

	getDetail(modelId) {
		return this.get(modelId, {});
	}

	addNewModel(dataBody) {
		return this.post('', dataBody);
    }
    
    editModel(body) {
        return this.put('', body);
    }

    deleteModel (modelId) {
        return this.delete(modelId);
    }

}

export default new ModelService();