'use strict';
import { ApiService } from '../core/services';

class DeviceService extends ApiService {

	constructor() {
		super('device');
	}

	getDetail(deviceId) {
		return this.get('/detail/'+deviceId, {});
	}

	editDeviceName(dataBody) {
		return this.put('', dataBody);
	}

}

export default new DeviceService();