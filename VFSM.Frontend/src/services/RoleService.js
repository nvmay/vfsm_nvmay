'use strict';
import { ApiService } from '../core/services';

class RoleService extends ApiService {

	constructor() {
		super('role');
	}

	search() {
		return this.get('search');
	}

	getDetail(Id) {
		return this.get('details/'+Id);
	}

	addNew(dataBody) {
		return this.post('create', dataBody);
	}
	editRole(dataBody) {
		return this.put('edit/'+dataBody.id, dataBody);
	}
	remove(id) {
		return this.delete('delete/'+id);
	}

	// editNickName(dataBody) {
	// 	return this.put('nick-name', dataBody);
	// }

	// exportUser(dataBody) {
	// 	return this.get('export', dataBody);
	// }

}

export default new RoleService();