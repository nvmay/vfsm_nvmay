'use strict';
import { ApiService } from '../core/services';

class SeriesService extends ApiService {

	constructor() {
		super('ActivitySeries');
	}

	getSeries(activityId) {
		return this.get('GetByActId/' + activityId, {});
	}

}

export default new SeriesService();