'use strict';

import { ApiService } from '../core/services';
class AuthenticationService extends ApiService {
    constructor(){
        super('account');
    }
    
    signIn (payload) {
        return this.post('signin', payload);
    }

    forgotPassword (payload) {
        return this.post('ForgotPassword', payload);
    }
}
export default new AuthenticationService();