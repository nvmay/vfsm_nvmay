
import React from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import ModalEvent from './ModalEvent';
import { Modal } from 'antd';
import VodaTable from '../../../common/VodaTable/VodaTable';
import DeviceGoogleMap from '../../../common/GoogleMap/DeviceGoogleMap';
import VodaButton from '../../../common/VodaButton/VodaButton';

import { DateTime } from '../../../core/utils';

const { formatTime, formatTimeToSend } = DateTime;

@inject('rootStore')
@observer
class SeriesHistory extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            showMap: false,
            latDevice: null,
            lngDevice: null,
            latBeacon: null,
            lngBeacon: null
        }
                
        this.columns = [{
            title: 'Created Date',
            dataIndex: 'createdDate',
            sorter: (a, b) => new Date(b.createdDate).getTime() - new Date(a.createdDate).getTime(),
            width: '18%',
        }, {
            title: 'Event Type',
            dataIndex: 'eventType',
            width: '18%',
        }, {
            title: 'Location Source',
            dataIndex: 'locationSource',
            width: '18%'
        }, {
            title: 'Location',
            dataIndex: 'location',
            width: '18%'
        }, {
            title: 'DisplayMsg',
            dataIndex: 'displayMsg',
            width: '18%'
        }, {
            title: 'Accuracy',
            dataIndex: 'accuracy',
            width: '10%'
        }];
        this.data = [];
    }

    componentDidMount() {
        // Get Battery detail
        const { activityId, rootStore } = this.props;
        rootStore.series.getSeries(activityId);
    }

    showGoogleMap = (obj) => {
        if (obj.logSource === 'DEVICE' || obj.logSource === 'LBS') {
            this.setState({
                showMap: true,
                latDevice: obj.lat,
                lngDevice: obj.lng,
                latBeacon: null,
                lngBeacon: null
            });
        } else {
            this.setState({
                showMap: true,
                latDevice: null,
                lngDevice: null,
                latBeacon: obj.lat,
                lngBeacon: obj.lng
            });
        }
    }

    handleBackToTable = () => {
        this.setState({
            showMap: false
        });
    }
    handleCloseMap = () => {
        this.setState({
            showMap: false,
            latDevice: null,
            lngDevice: null,
            latBeacon: null,
            lngBeacon: null
        });
    }

    renderMap = () => {
        const { latDevice, lngDevice, latBeacon, lngBeacon, showMap } = this.state;
        const datalocation = [];
        if (latDevice !== null && latDevice !== undefined  && lngDevice !== null && lngDevice  !== undefined ) {
            datalocation.push(
                {
                    title: 'Device',
                    location: {
                        lat: latDevice ,
                        lng: lngDevice ,
                    }
                }
            )
        }
        if (latBeacon !== null && latBeacon !== undefined && lngBeacon !== null && lngBeacon !== undefined) {
            datalocation.push(
                {
                    title: 'Beacon',
                    location: {
                        lat: latBeacon,
                        lng: lngBeacon,
                    }
                }
            )
        }
        return datalocation.length > 0 
            ? 
            (
                <Modal
                    width={1200}
                    footer={null}
                    visible={showMap}
                    onCancel={this.handleCloseMap}
                    className="see-more-modal" 
                >
                    <DeviceGoogleMap data={datalocation} />
                </Modal>
            )
            : (
                <Modal
                    width={1200}
                    footer={null}
                    visible={showMap}
                    onCancel={this.handleCloseMap}
                    className="see-more-modal"
                >
                    <div style={{ fontSize: '14px', fontWeight: 200, padding: '50px 210px', fontStyle: 'italic' }}>This device have not location</div>
                </Modal>
            )
    }

    render() {

        const dataDetail = this.props.rootStore.series.seriesData;
        this.data = [];
        dataDetail && dataDetail.map((obj, index)=> {
            this.data.push({
                key: obj.actSeriesSeq,
                createdDate: formatTime(obj.createdDate),
                eventType: obj.action.toUpperCase(),
                locationSource: obj.logSource,
                location: <VodaButton onClick={() => this.showGoogleMap(obj)} text="See map" iconType="Location" ></VodaButton>,
                displayMsg: obj.message,
                accuracy: obj.accuracy
            });
        });

        return (

            <div className="modal-event">
                {
                    !this.state.showMap 
                    ? <VodaTable columns={this.columns} data={this.data} scroll={{ y: 460 }} />
                    : this.renderMap()
                }
            </div>
        )
    }

}

SeriesHistory.propTypes = {
    activityId: PropTypes.string.isRequired
};
export default SeriesHistory;