
import React from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import ModalEvent from './ModalEvent';
import { DateTime } from '../../../core/utils';

const { formatTime, formatTimeToSend } = DateTime;
@inject('rootStore')
@observer
class SOSModal extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        // Get SOS detail
        const { deviceId, rootStore } = this.props;
        rootStore.event.getDetail(deviceId, { type: 'SOS' });
    }

    render() {

        const dataDetail = this.props.rootStore.event.eventDetail;
        
        const columns = [{
            title: 'Created Date',
            dataIndex: 'createdDate',
            sorter: (a, b) => new Date(b.createdDate).getTime() - new Date(a.createdDate).getTime(),
            width : '20%',
        }, {
            title: 'Activity ID',
            dataIndex: 'activityId',
            width : '20%'
        }, {
            title: 'Location Source',
            dataIndex: 'locationSource',
            width: '20%'
        }];

        const data = [];

        dataDetail && dataDetail.map((obj, index)=> {
            data.push({
                key: index,
                createdDate: formatTime(obj.createdDate),
                deviceId: obj.deviceId,
                activityId: obj.activityId,
                accuracy: obj.accuracy,
                locationSource: obj.locSource,
                lat: obj.lat,
                lng: obj.lng,
                latBeacon: obj.latBeacon,
                lngBeacon: obj.lngBeacon
            });
        });

        return (

            <ModalEvent columns={columns} data={data} haveSeries={true}/>
        )
    }

}

SOSModal.propTypes = {
    deviceId: PropTypes.string.isRequired
};
export default SOSModal;