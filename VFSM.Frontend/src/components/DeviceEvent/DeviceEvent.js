import React from 'react';
import { inject, observer } from 'mobx-react';
import EventCard from './EventCard/EventCard';
import VodaBody from '../../common/VodaBody/VodaBody';
import DatePicker from '../../common/DatePicker/DatePicker';
import moment from 'moment';
import Select from '../../common/Select/Select';
import { Input, Row, Col } from 'antd';
import { DateTime } from '../../core/utils';
import VodaButton from '../../common/VodaButton/VodaButton';

import './styles.scss';

const { formatTime, formatTimeToSend } = DateTime;

const pageSize = 15;
@inject('rootStore')
@inject('apiStore')
@observer
class DeviceEvent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            openCustomer: false,
            openDevice: false,
            disableInput: true,
            startDate: moment().subtract(6, 'months'),
            endDate: moment(),
            sortType: undefined,
            orderType: undefined,
            page: 1,
            searchType: undefined,
            searchInput: '',
            customerId: undefined,
            deviceId: undefined
        };
        this.option = [
            {
                label: 'IMEI',
                value: 'imei'
            },
            {
                label: 'Customer ID',
                value: 'userId'
            },
            {
                label: 'Device ID',
                value: 'deviceId'
            },
            {
                label: 'Beacon MAC',
                value: 'mac'
            },
            {
                label: 'Event Type',
                value: 'eventType'
            },
            {
                label: 'Activity ID',
                value: 'activityId'
            }
        ];
        this.optionSort = [
            {
                label: 'Activation Date',
                value: 'activatedDate'
            },
            {
                label: 'Imei',
                value: 'imei'
            }
        ];
    }

    componentDidMount() {
        const fromDate = formatTimeToSend(this.state.startDate);
        const toDate = formatTimeToSend(this.state.endDate);
        const body = {
            fromDate: fromDate,
            toDate: toDate,
            CurrentPage: 1,
            PageSize: pageSize
        };
        this.props.rootStore.event.getAll(body);
    }

    // Change page
    changePage = (pageNumber) => {
        this.setState({ page: pageNumber }, () => this.handleSearch());
    }

    // Sorting
    handleSort = (value) => {
        this.setState({ sortType: value }, () => this.handleSearch());
    }

    handleSortOrder = (type) => {
        this.setState({ orderType: type }, () => this.handleSearch());
    }

    // Change Search type
    handleChangeSearchType = (value) => {
        this.setState({ searchType: value });
        if (!value) {
            this.setState({ disableInput: true, searchInput: '' });
        } else {
            this.setState({ disableInput: false });
        }
    }

    // Change Input when search
    handleChangeSearchInput = (e) => {
        this.setState({ searchInput: e.target.value });
    }

    // Change From date
    handleChangeStartDate = (date) => {
        this.setState({
            startDate: date
        });
    }

    // Change To date
    handleChangeEndDate = (date) => {
        this.setState({
            endDate: date
        });
    }

    // Click search button
    handleSearch = () => {
        const { searchType, searchInput, page, startDate, endDate, sortType, orderType } = this.state;
        const body = {
            fromDate: formatTimeToSend(startDate),
            toDate: formatTimeToSend(endDate),
            CurrentPage: page,
            PageSize: pageSize,
            sort: sortType,
            sortAsc: orderType
        }
        body[searchType] = searchInput;
        this.props.rootStore.event.getAll(body);
    }

    render() {
        const { loading } = this.props.apiStore;
        const { rootStore } = this.props;
        const events = rootStore.event.events;

        const body = <div>
            {
                events.items && events.items.length > 0
                    ? <Row gutter={16}>
                        {
                            events.items.map(event => {
                                return (<Col className="gutter-row" span={8} key={event.deviceId}>
                                    <EventCard device={event} />
                                </Col>)
                            })
                        }
                    </Row>
                    : loading ? <div></div> :<div style={{ fontSize: '30px', fontWeight: 500, textAlign: 'center', padding: '50px', fontStyle: 'italic' }}>No found the result</div>
            }

        </div>

        return (

            <div className="device-event-root">
                <div className="row-end">
                    <DatePicker
                        label="From"
                        className="date-from"
                        selected={this.state.startDate}
                        onChange={this.handleChangeStartDate}
                    />
                    <DatePicker
                        label="To"
                        className="date-to ml-1"
                        selected={this.state.endDate}
                        onChange={this.handleChangeEndDate}
                    />
                    <Select className="ml-1" placeholder="Type: Search type" option={this.option} onChange={this.handleChangeSearchType} />
                    <Input disabled={this.state.disableInput} className="ml-1" placeholder="Select Search Type" onChange={this.handleChangeSearchInput} />
                    <VodaButton text="Search" iconType="Search" onClick={this.handleSearch} />
                </div>
                <VodaBody
                    optionSort={this.optionSort}
                    onChangeSort={this.handleSort}
                    body={body}
                    pageCount={events.pageCount}
                    onChangePage={this.changePage}
                    onSortOrder={this.handleSortOrder}
                />

            </div>
        );
    }
}

export default DeviceEvent;