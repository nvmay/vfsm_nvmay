import React, { Component } from 'react';
import PropTypes from 'prop-types';
// import { Link } from 'react-router-dom';
// import { observer } from 'mobx-react';
import './styles.scss'
import { Avatar, Row, Col, Popover } from 'antd';
import User from './images/User.svg';
import Android from './images/Android@2x.svg';
import iOs from './images/iOs@2x.svg';

class CustomerCard extends Component {

    handleClick = () => {
        const { id } = this.props;
        this.props.handleClickAvatar(id);
    }

    render() {
        const { id, type, name } = this.props;
        return (
            <Row gutter={8} className="customer">
                <Col className="gutter-row" span={7}>
                    <span className="customer-text">Customer</span>
                </Col>
                <Col className="gutter-row customer-detail-show" span={10}>
                    <div className="customer-avatar" onClick={this.handleClick}>
                        <Avatar size={62} src={User} />
                    </div>
                    <Popover placement="bottom" content={id} trigger="hover">
                        <div className="customer-code">
                            <span>{id}</span>
                        </div>
                    </Popover>
                </Col>
                <Col className="gutter-row device-column" span={7}>
                    <div className="customer-mobile-type">
                        {type.toLowerCase() === "android"
                            ? <Avatar className="customer-device" size={42} src={Android} />
                            : type.toLowerCase() === "ios"
                                ? <Avatar className="customer-device" size={42} src={iOs} />
                                : ''}
                    </div>
                </Col>
            </Row>
        );
    }

};

CustomerCard.propTypes = {
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
}

CustomerCard.propDefault = {
    handleClickAvatar: () => { }
};

export default CustomerCard;
