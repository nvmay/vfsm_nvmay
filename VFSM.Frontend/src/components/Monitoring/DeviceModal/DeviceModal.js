
import React from 'react';
import { Row, Col } from 'antd';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import DeviceGoogleMap from '../../../common/GoogleMap/DeviceGoogleMap';
import ColModal from '../../../common/ColModal/ColModal';
import { DateTime } from '../../../core/utils';
import './styles.scss';

const { formatTimeDetail } = DateTime;
@inject('rootStore')
@observer
class DeviceModal extends React.Component {

    componentDidMount() {
        // Get employee detail
        const { device, deviceId, rootStore } = this.props;
        if (deviceId) {
            rootStore.device.getDetail(deviceId);
        } else {
            rootStore.device.getDetail(device.deviceId);
        }
    }

    handleCancel = () => {
        this.props.onClose();
    }

    showbattery = (data) => {
        return (
            <div>
                <span>{data.status ? data.status.replace(/["[\]]/g, '') : '--' } </span>
                <span
                    className = { (data.battery && data.battery) <= 20 ? 'vfsm-device-status-low-battery' : '' }
                >
                    {data.battery ? '(' + data.battery + '%)' : '' }
                </span>
            </div>
        )
    }

    render() {
        const dataDetail = this.props.rootStore.device.deviceDetail;
        const datalocation = [];
        // let lastAct;
        // if (this.props.deviceId) {
        //     lastAct = dataDetail;
        // } else {
        //     lastAct = this.props.rootStore.event.lastEventDetail
        // }
        if (dataDetail.lat !== null && dataDetail.lat !== undefined && dataDetail.lng !== null && dataDetail.lng !== undefined) {
            datalocation.push(
                {
                    title: 'Device',
                    location: {
                        lat: dataDetail.lat,
                        lng: dataDetail.lng
                    }
                }
            )
        }
        if (dataDetail.latBeacon !== null && dataDetail.latBeacon !== undefined && dataDetail.lngBeacon !== null && dataDetail.lngBeacon !== undefined) {
            datalocation.push(
                {
                    title: 'Beacon',
                    location: {
                        lat: dataDetail.latBeacon,
                        lng: dataDetail.lngBeacon
                    },
                    beaconMac: dataDetail.beaconMac ? dataDetail.beaconMac : '--'
                }
            )
        }

        const data = [
            {
                title: 'Activation Date',
                content: (dataDetail.activatedDate && formatTimeDetail(dataDetail.activatedDate)) || '--'
            },
            {
                title: 'Device ID',
                content: dataDetail.deviceId
                    ? dataDetail.deviceId
                    : '--'
            },
            {
                title: 'Version',
                content: dataDetail.deviceVersion || '--'
            },
            {
                title: 'Imei',
                content: dataDetail.imei || '--'
            },
            {
                title: 'Status',
                content: dataDetail.status ? dataDetail.status.replace(/["[\]]/g, '') : '--'
            }
        ];

        return (
            <Col className="device-modal">
                {
                    data.map((obj, index) => {
                        return (
                            <ColModal data={obj} key={index} />
                        )
                    })
                }
                <Row className="customer-detail-row">
                    {
                        (datalocation.length > 0)
                            ? <DeviceGoogleMap data={datalocation} />
                            : <div style={{ fontSize: '14px', fontWeight: 200, padding: '50px 210px', fontStyle: 'italic' }}>Location not found</div>
                    }
                    <div id="device-detail-col-map">
                        <span className="customer-detail-col-name-span"> Location </span>
                    </div>
                </Row>
            </Col>
        )
    }

}

DeviceModal.propTypes = {
    deviceId: PropTypes.string,
    device: PropTypes.object
}

export default DeviceModal;