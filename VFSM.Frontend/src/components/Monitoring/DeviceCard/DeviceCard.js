import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
// import { observer } from 'mobx-react';
import './styles.scss'
import { Avatar, Modal, Popover } from 'antd';
import PropTypes from 'prop-types';
import SmartDevice from './images/SmartDevice.svg';
import SmallSelect from '../../../common/SmallSelect/SmallSelect';
import AddMoreDeviceModal from '../AddMoreDeviceModal/AddMoreDeviceModal';
import { DateTime } from '../../../core/utils';
import SeeMoreModal from '../SeeMoreModal/SeeMoreModal';

const { formatTimeDetail } = DateTime;
class DeviceCard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openAddDevice: false,
            openSeeMore: false
        };
        this.componentAvatar = [];
    }

    handleClick = (device) => {
        this.props.handleClickDevice(device);
    }

    clickMenu = (name) => {
        if (name === 'See more device') {
            this.handleSeeMore();
        } else {
            this.handleAddMore();
        }
    }

    handleSeeMore = () => {
        this.setState({ openSeeMore: true });
    }

    handleCloseSeeMore = () => {
        this.setState({ openSeeMore: false });
    }

    renderSeeMoreModal = () => {
        const { openSeeMore } = this.state;
        const { deviceList } = this.props;
        let data = [];
        deviceList && deviceList.map((obj, index)=> {
            data.push({
                key: index,
                activatedDate: obj.activatedDate ? formatTimeDetail(obj.activatedDate) : '',
                deviceId: obj.deviceId,
                imei: obj.imei,
                deviceVersion: obj.deviceVersion,
                role: obj.role,
                status: obj.status ? obj.status.replace(/["[\]]/g, '') : '',
                lat: obj.lat,
                lng: obj.lng,
                latBeacon: obj.latBeacon,
                lngBeacon: obj.lngBeacon,
                beaconMac: obj.beaconMac
            });
        });
        if (openSeeMore) {
            return (
                <Modal
                    width={1200}
                    footer={null}
                    title={"Device List [ Customer ID : " + this.props.userId + " ]"}
                    visible={openSeeMore}
                    onCancel={this.handleCloseSeeMore}
                    className="see-more-modal"
                >
                    <SeeMoreModal data={data} />
                </Modal>
            )
        }
        return null;
    }

    handleAddMore = () => {
        this.setState({ openAddDevice: true });
    }

    handleCloseAddModal = () => {
        this.setState({ openAddDevice: false });
    }

    renderAddModal = () => {
        const { openAddDevice } = this.state;
        if (openAddDevice) {
            return (
                <Modal
                    width={693}
                    footer={null}
                    title="Add New Device"
                    visible={openAddDevice}
                    onCancel={this.handleCloseAddModal}
                >
                    <AddMoreDeviceModal userId={this.props.userId} onClose={this.handleCloseAddModal} />
                </Modal >
            )
        }
        return null;
    }

    renderAvatar = () => {
        this.componentAvatar = [];
        const { deviceList } = this.props;
        deviceList.map(device => {
            return device.role === 'ROOT'
                && this.componentAvatar.push(
                    <Popover placement="bottom" content={device.imei} trigger="hover" key={device.deviceId}>
                        <div
                            className="device-avatar-root-child"
                            onClick={() => this.handleClick(device)}>
                            <Avatar shape="square" src={SmartDevice} />
                        </div>
                    </Popover>
                );
        });
        deviceList.map(device => {
            return device.role === 'SUB'
                && this.componentAvatar.push(
                    <Popover placement="bottom" content={device.imei} trigger="hover" key={device.deviceId}>
                        <div
                            className="device-avatar-sub-child"
                            onClick={() => this.handleClick(device)}>
                            <Avatar shape="square" src={SmartDevice} />
                        </div>
                    </Popover>
                );
        });
        return this.componentAvatar.splice(0, 5);
    }

    render() {
        const selectOption = [
            {
                id: '0',
                title: 'See more device'
            },
            {
                id: '1',
                title: 'Add more device',
                disabled: this.props.menuRole !== 'operation.write' ? true : false
            }
        ];

        return (
            <div className="device">
                <span className="device-text">Device</span>
                <div className="device-avatar">
                    {this.renderAvatar()}
                </div>
                <SmallSelect selectOption={selectOption} handleClickMenuCallBack={this.clickMenu} />
                {this.renderAddModal()}
                {this.renderSeeMoreModal()}
            </div >
        );
    }

};

DeviceCard.propTypes = {
    deviceList: PropTypes.object.isRequired,
    userId: PropTypes.string.isRequired,
    handleClickDevice: PropTypes.func.isRequired
}

export default DeviceCard;
