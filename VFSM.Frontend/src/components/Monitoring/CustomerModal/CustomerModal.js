
import React from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { Col } from 'antd';
import ColModal from '../../../common/ColModal/ColModal';
import { DateTime } from '../../../../src/core/utils';

import './styles.scss';

const { formatTimeDetail } = DateTime;

@inject('rootStore')
@observer
class CustomerModal extends React.Component {


    componentDidMount() {
        // Get employee detail
        const { customerId, rootStore } = this.props;
        rootStore.user.getDetail(customerId);
    }

    handleCancel = () => {
        this.props.onClose();
    }

    render() {

        const dataDetail = this.props.rootStore.user.userDetail;
        const rootCount = dataDetail.rootCount || 0;
        const subCount = dataDetail.subCount || 0;
        const data = [
            {
                title: 'Created Date',
                content: dataDetail.createdDate
                    ? formatTimeDetail(dataDetail.createdDate)
                    : '--'
            },
            {
                title: 'Customer ID',
                content: dataDetail.userId
                    ? dataDetail.userId
                    : '--'
            },
            {
                title: 'Mobile Type',
                content: dataDetail.mobileType || '--'
            },
            {
                title: 'Device Count',
                content: 'Care taker (' + rootCount + '), Invitee(' + subCount + ')'
            }
        ];

        return (

            <div className="customer-modal">
                <Col>
                    {
                        data.map(obj => {
                            return (
                                <ColModal data={obj} key={obj.title} />
                            )
                        })
                    }
                </Col>
            </div>
        )
    }

}

CustomerModal.propTypes = {
    customerId: PropTypes.string.isRequired
};
export default CustomerModal;