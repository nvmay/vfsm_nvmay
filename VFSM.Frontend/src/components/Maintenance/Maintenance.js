import React from 'react'; 
import { Switch, Route } from 'react-router-dom';
import ModelRouter from  './ModelRouter/ModelRouter';
export  default class Maintenance extends React.Component{

    switchRoutes = () => {
        const { match } = this.props;
        return (<Switch>
            {
                ModelRouter.map(prop => <Route exact={prop.exact} path={`${match.path}/${prop.path}`} component={prop.component} key={prop.path} />)
            }
        </Switch>);
    };
    
    render(){
        return (
            <div>
                {this.switchRoutes()}
            </div>
        )
    }
}