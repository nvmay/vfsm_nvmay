import React from 'react';
import { inject, observer } from 'mobx-react';
import { Modal, Input, Col, Row } from 'antd';
import moment from 'moment';
import ModelCard from '../ModelCard/ModelCard';
import VodaButton from '../../../common/VodaButton/VodaButton';
import DatePicker from '../../../common/DatePicker/DatePicker';
import Select from '../../../common/Select/Select';
import IconSvg from 'common/icon/IconSvg';
import VodaBody from '../../../common/VodaBody/VodaBody';
import { DateTime } from '../../../core/utils';
import ModelAdd from '../ModelAdd/ModelAdd';
import AddCard from '../../../../src/common/AddCard/AddCard';

import './styles.scss';

const { formatTime, formatTimeToSend } = DateTime;
const pageSize = 100;
@inject('rootStore')
@inject('apiStore')
@inject('menuRoleStore')
@observer
class MaintenanceContainer extends React.Component {

    constructor(props){
        super(props);
        this.state={
            isOpenDetail: false,
            sortAsc: true,
            isOpenModelAdd: false,
            idOTA: null,
            searchButtonDisabled: true,
            isopenOTA : false,
            openModelDetail: false,
            idDetail: null,
            startDate: '',
            endDate: '',
            searchInput: '',
            sort: '',
            disableOrderSort: false
        }
    }

    componentDidMount() {
        const fromDate = formatTimeToSend(this.state.startDate);
        const toDate = formatTimeToSend(this.state.endDate);
        const { sortAsc } =  this.state;
        const body = {
            //fromDate: fromDate,
            //toDate: toDate,
            sortAsc: sortAsc,
            CurrentPage: 1,
            PageSize: pageSize
        };
        this.props.rootStore.model.getAll(body);
    }

    /*  CallBack funcs*/
    //open OTA page callback
    handleClickOpenOTACallBack = (id) => {
        this.props.history.push(`/main/Maintenance/OTA?modelId=${id}`);
    }

    handleBackToModelsCallBack = () => {
        this.setState({isopenOTA: false})
    }

    handleCancelCallBack = () => {
        this.setState({
            isOpenModelAdd: false
        })
    }
    
    handleCloseModelModal = () => {
        this.setState({ openModelDetail : false })
    }

    handleCheckChangeDate = () => {
        const { startDate, endDate } = this.state;
        if(startDate || endDate ){
            this.setState({
                searchButtonDisabled: false,
            })
        }
    }

    handleChangeStartDate = (value) => {
        this.setState({
            startDate: value,
        }, () => this.handleCheckChangeDate());
    }

    handleChangeEndDate = (value) => {
        this.setState({
            endDate: value,
        },  () => this.handleCheckChangeDate());
    }

    handleSortOrder = (value) => {
        this.setState({
            sortAsc : value
        }, () => this.handleClickSearch());
    }

    handleSort = (value) => {
        this.setState({
            sort: value,
        }, () => this.handleClickSearch())
    }

    checkInputSearch = () => {
        const {searchInput,endDate , startDate } = this.state;
        if(searchInput === ''){
            this.setState({
                searchButtonDisabled: true,
            })
        } else {
            this.setState({
                searchButtonDisabled: false,
            })
        }
    }

    handleInputOnChange = (value) => {
        this.setState({
            searchInput: value.target.value
        }, () => this.checkInputSearch());
    }

    handleOnPressEnter =(value)=> {
        this.setState({
            searchInput: value.target.value
        }, () => this.handleClickSearch());
    }

    handleClickSearch = () => {
        const { startDate, endDate, searchInput, sort, sortAsc } = this.state;
        const fromDate = formatTimeToSend(startDate);
        const toDate = formatTimeToSend(endDate);
        if(searchInput === '' ) {
            const body = {
                fromDate: fromDate,
                toDate: toDate,
                sort:sort,
                sortAsc: sortAsc,
                CurrentPage: 1,
                PageSize: pageSize
            };
            this.props.rootStore.model.getAll(body);
        } else {
            const body = {
                modelId: searchInput,
                fromDate: fromDate,
                toDate: toDate,
                sort:sort,
                sortAsc: sortAsc,
                CurrentPage: 1,
                PageSize: pageSize
            };
            this.props.rootStore.model.getAll(body);
        }
        
    } 

    

    /* Sync after adding */
    handleSyncCallBack = () => {
        //this.handleClickSearch();
    }

    handleCloseModelAdd = () =>{ 
        this.setState({
            isOpenModelAdd: false
        });
    }

    handleAddModel = () =>{
        this.setState({
            isOpenModelAdd: true
        })
    }

    renderModelAdd = () => {
        const {isOpenModelAdd} = this.state;
        if(isOpenModelAdd) {
            return (
                <Modal 
                width={693}
                footer={null}
                title="Model New"
                visible={isOpenModelAdd}
                onCancel={this.handleCloseModelAdd}
            >
                <ModelAdd 
                    handleCancelCallBack={this.handleCancelCallBack} 
                />
            </Modal>
            )
        }
        return null;
    }

    render() {
        const { loading } = this.props.apiStore;
        const { menuRoles } = this.props.menuRoleStore;
        const { searchButtonDisabled, isDisableInput } = this.state; 
        const dataFiltered = this.props.rootStore.model.models.items ? this.props.rootStore.model.models.items : [] ;
        const optionSort = [
            {
                label: 'Model ID',
                value: 'ModelId'
            },
            {
                label: 'Name',
                value: 'Name'
            },
        ];

        const body = 
        <Row gutter={24} style={{paddingBottom: 20}}>
        {
            dataFiltered && dataFiltered.length > 0 
            ? (
                <div className="vfsm-maintenance-body-container">
                    {
                        dataFiltered.map( (prop, index) =>
                            <Col className="gutter-row" span={6}  >
                                <ModelCard 
                                    data={prop}
                                    modelId={prop.modelId}
                                    key={index}
                                    handleClickOpenOTACallBack = { () => this.handleClickOpenOTACallBack(prop.modelId)}
                                    handleClickMenuCallBack = {this.handleClickMenuCallBack}
                                    menuRole= {menuRoles.maintenance}
                                />
                            </Col>
                        )
                    }
                    {
                        menuRoles && menuRoles.maintenance == 'operation.write' ?
                        (<Col className="gutter-row" span={6} >
                            <AddCard handleClick={this.handleAddModel} />
                        </Col>): null
                    }
                </div>
                )   
            :
            (
                loading ? <div></div> :<div style={{ fontSize: '30px', fontWeight: 500, textAlign: 'center', padding: '50px', fontStyle: 'italic' }}>No found the result</div>
            ) 
        }
        </Row>

        return (
            <div className="vfsm-maintenance-container">
                <div className="vfsm-maintenance-header">
                    <div className="row-end">
                        <DatePicker
                        label="From"
                        className="date-from"
                        selected={this.state.startDate}
                        onChange={this.handleChangeStartDate}
                        />
                        <DatePicker
                            label="To"
                            className="date-to ml-1"
                            selected={this.state.endDate}
                            onChange={this.handleChangeEndDate}
                        />
                        <Input  style={{marginLeft: '15px', width: '271px'}}
                            placeholder={"Search"}
                            onPressEnter={this.handleOnPressEnter}
                            onChange={this.handleInputOnChange} />
                        <VodaButton text="Search" iconType="Search" onClick={this.handleClickSearch} />
                    </div>
                </div>
                <div className="vfsm-maintenance-body">
                    <VodaBody 
                        optionSort={optionSort}
                        onChangeSort={this.handleSort}
                        body={body}
                        onSortOrder={this.handleSortOrder}
                    />
                </div>
                {this.renderModelAdd()}
            </div>
        )
    }
}

export default MaintenanceContainer;