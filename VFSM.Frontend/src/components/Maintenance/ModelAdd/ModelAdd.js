
import React from 'react';
import { inject, observer } from 'mobx-react';
import { Input } from 'antd';
import Constant from  '../../../core/utils/Constant';
import InfoBox from '../../../common/InfoBox/InfoBox';
import ButtonFooter from '../../../common/ButtonFooter/ButtonFooter';
import VodaInput from '../../../common/Input/VodaInput';
import Notification   from '../../../core/ui/Notification/Notification';
import './styles.scss';
const { onOpenNotification } = Notification;
const { TextArea } = Input;
const { USER_ERROR, API_STATUS} = Constant;

@inject('rootStore')
@observer

class ModelAdd extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            disabled: true,
            isShowError: false,
            errorMsg: '',
            modelId: '',
            modelName: '',
            modelDescription: '',
            loading: false,
        }
    }

    /**/
    handleCheckInput = () => {
        const {modelId, modelName, isShowError} = this.state;
        ( modelId && modelName && modelId !== '' && modelName !== '' && !isShowError ) 
            ? (this.setState({ disabled: false }))
            : (this.setState({ disabled: true}))
    }

    handleInputModelId = (value) => {
        this.setState({
            modelId: value.target.value,
            isShowError: false,
            disabled: false,
        }, this.handleCheckInput);
    }

    handleInputName = (value) => {
        this.setState({
            modelName : value.target.value
        }, this.handleCheckInput);
    }

    handleInputDescription = (value) => {
        this.setState({
            modelDescription: value.target.value 
        }, this.handleCheckInput)
    }
    /**/

    handleCancel = () => {
        this.setState({
            modelId: '',
            modelName: '',
            modelDescription: '',
            isShowError: false,
            disabled : true,
            loading: false,
        },this.props.handleCancelCallBack());
    }

    handleAddModelCallBack = () => {
        const addModelStatus = this.props.rootStore.model.addModelStatus ? this.props.rootStore.model.addModelStatus : '';
        let addNoti = '', addStatus = '';
        if(addModelStatus.status.toUpperCase() === 'SUCCESS') {
            this.setState({
                modelId: '',
                modelName: '',
                modelDescription: '',
                loading: false,
                disabled : true
            },this.handleCancel());
            addNoti = 'Add Successful';
            addStatus = API_STATUS.SUCCESS;
        } else {
            this.setState({
                isShowError: true,
                loading: false,
                disabled: true,
                errorMsg : "Model ID existed"
            })
            addNoti = 'Add Unsuccessful';
            addStatus = API_STATUS.SUCCESS;
        }
        this.openNotification(addNoti, addStatus);
    }

    handleClickFirst = () => {
        const {modelId, modelName, modelDescription } = this.state;
        const newModel = {
            modelId : modelId,
            name : modelName,
            description : modelDescription,
        }
        this.setState({
            disabled: true,
            isShowError: false ,
            loading: true
        })
        this.props.rootStore.model.addNewModel(newModel , this.handleAddModelCallBack);
    }

    /* notification */
    openNotification = (msg, status) =>{
        onOpenNotification(msg, status);
    }

    render(){
        const { isShowError, errorMsg } = this.state;
        const data = [
            {
                "title": "Model ID",
                "content" :  <div className="modal-col-input-wapper">
                                <VodaInput className="add-device-input" placeholder="Type" 
                                onChange={this.handleInputModelId} 
                                value={this.state.modelId}
                                options={{
                                    maxLength: 10,
                                    isShowError:isShowError,
                                    errorMsg: errorMsg,
                                }}
                            />
                            </div>,
                "hasInput" : true
            },
            {
                "title": "Name",
                "content" :  <VodaInput className="add-device-input" 
                                placeholder="Type" 
                                onChange={this.handleInputName}
                                value={this.state.modelName}
                                options={{
                                    maxLength: 24,
                                }}
                             />,
                "hasInput" : true
            },
            {      
                "title": "Description",
                "content" :  <VodaInput 
                                className="add-device-input" 
                                placeholder="Type" 
                                onChange={this.handleInputDescription} 
                                value={this.state.modelDescription}
                                options={{
                                    maxLength: 50,
                                }}
                              />,
                "hasInput" : true
            }
   
        ];
        return(
            <div className="modal-add-model-wapper">
                <InfoBox data={data} />
                <ButtonFooter 
                    text="Add" 
                    className="model-add-button"
                    disabled={this.state.disabled}
                    loading={this.state.loading}
                    handleClickCancel={this.handleCancel}
                    handleClickFirst={this.handleClickFirst}
                />
            </div>
        )
    }
}

export default  ModelAdd;

