import React from 'react';
import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import InfoBox from 'common/InfoBox/InfoBox';
import ButtonFooter from 'common/ButtonFooter/ButtonFooter';
import VodaInput from 'common/Input/VodaInput';
import SelectBox from 'common/Select/Select';
import Roles from 'core/utils/RoleTable';
import Notification   from '../../../../core/ui/Notification/Notification';
import Constant from  '../../../../core/utils/Constant';
import { checkInputExit} from '../../../../core/utils/validate';
import './styles.scss';

const { role } = Roles;
const { USER_ERROR, API_STATUS} = Constant;
const { onOpenNotification } = Notification;
@inject('rootStore')
@inject('apiStore')
@inject('menuRoleStore')
@observer
class Detail extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            disabled: false,
            isInputNameShowError : false,
            isInputDescriptionShowError: false,
            dashboardPolicy:  '',
            isEdit : false,
            roleName: {
                value: '',
                isShowError: false,
                errorMsg: '',
            },
        }
        this.roleOption = [
            {
                value: role.Read,
                label: 'Read'
            },
            {
                value: role.Write,
                label: 'Write'
            },
            {
                value: role.Hide,
                label: 'Hide'
            }
        ];
    }
    
    componentDidMount (){
        
    }

    hanldeCheckInputChange = () => {
        const { roleName  } = this.state;
        if(checkInputExit(roleName.value)){
            this.setState({
                roleName:{
                    ...this.state.roleName,
                    isShowError: false,
                    errorMsg: USER_ERROR.EMPTY,
                },
                disabled: false,
            });
        } else {
            this.setState({
                roleName:{
                    ...this.state.roleName,
                    isShowError: true,
                    errorMsg: USER_ERROR.REQUIRED,
                },
                disabled: true,
            });
        }
        // if( newName !== ''){
        //     this.setState({
        //         disabled: false,
        //     })
        // }
    }

    handleInputName = (value) =>{
        this.setState({
            roleName:{
                ...this.state.roleName,
                value: value.target.value,
                isShowError: false,
                errorMsg: USER_ERROR.EMPTY,
            }
        }, () =>  this.hanldeCheckInputChange())
        // this.setState({
        //     disabled: false
        // });
        // this.props.rootStore.role.setName(value.target.value);
    }
    
    handleInputDescription = (value) =>{
        this.setState({
            disabled: false
        });
        this.props.rootStore.role.setDescription(value.target.value);
    }
    handleInputDas = (value) =>{
        this.setState({
            dashboardPolicy: value.target.value
        }, this.hanldeCheckInputChange());
    }

    handleEditModelCallBack = (status) => {
        // const { isEdit } = this.state;
        let msgNoti = '', detailStatus= '';
        
        if( status && status.toUpperCase() === API_STATUS.SUCCESS){
            this.setState({
                isEdit: false,
                disabled: true,
                roleName: {
                    ...this.state.roleName,
                    isShowError: false,
                    errorMsg: USER_ERROR.EMPTY,
                },
            })
            this.handleCancel();
            msgNoti = 'Update Successful';
            detailStatus = API_STATUS.SUCCESS;
        } else  if ( status && status.toUpperCase() === API_STATUS.EXITED) {
            this.setState({
                isEdit: true,
                roleName: {
                    ...this.state.roleName,
                    isShowError: true,
                    errorMsg: 'This name has exited',
                },
                disabled : true
         })
            msgNoti = 'Update Unsuccessful';
            detailStatus = API_STATUS.ERROR;
            //this.handleClickCancelCallback();
        } else {
            this.setState({
                isEdit: false,
                disabled : true
            });
            this.handleCancel();
            msgNoti = 'Update Unsuccessful';
            detailStatus = API_STATUS.ERROR;
        }
        this.openNotification(msgNoti, detailStatus);
    }

    handleSaveChange = () => {
        const { roleName } = this.state;
        // set Data
        this.props.rootStore.role.setName(roleName.value);
        // call API
        this.props.rootStore.role.editRole(this.handleEditModelCallBack);
    }

    handleClickFirst = () => {
        const roleDetail = this.props.rootStore.role.roleDetail;
        const { isEdit } = this.state;
        if (isEdit === false) {
            this.setState({
                roleName: {
                    ...this.state.roleName,
                    value: roleDetail.name
                }
            })
        };
        this.setState((prevSate) => ({
            isEdit: {
                ...prevSate.isEdit,
                isEdit : !prevSate.isEdit
            },
        }), () => {
            if(!isEdit) {
                this.setState({
                    disabled: true,
                })
            } else {
                this.handleSaveChange();
            }
        });
    }

    handleCancel =() =>{
        // const { isEdit } = this.state;
        const status = this.props.rootStore.role.addStatus ? this.props.rootStore.role.addStatus.toUpperCase() : '';
        // const hasChange = status === 'SUCCESS' ? true : false ;
        this.setState({
            isEdit: false,
            disabled: false,
        }, () => this.props.handleClickCancelCallback());
        // this.props.handleClickCancelCallback();
    }
    handleSelect =(value, id) =>{
        this.setState({
            disabled: false
        });
        if(value !== undefined){
            this.props.rootStore.role.setOperation(value, id);
        }
    }

    openNotification = (msg, status) =>{
        onOpenNotification(msg, status);
    }

    render(){
        // const { id } = this.props;
        // const { isEdit, isLoading, isInputNameShowError, isInputDescriptionShowError  } = this.state;
        const { isEdit, roleName } = this.state;
        const roleDetail = this.props.rootStore.role.roleDetail;
        const {loading} = this.props.apiStore;
        const { menuRoles } = this.props.menuRoleStore;
        const isAdmin = (roleDetail.name && roleDetail.name.toUpperCase() === 'ADMIN') ?  true : false ;
        let dData = [];
        const hData = [
                {
                    "title"  : 'Role ID',
                    "content": ( roleDetail.id ? <span className="modal-col-input-content">{roleDetail.id}</span> : '--'),
                    'hasFooter': true,
                    "hasInput" : false
                },
                {
                    "title"  : 'Name',
                    "content":  isEdit ? (<div className="modal-col-input-wapper">
                                            <VodaInput className="add-device-input" 
                                                onChange={this.handleInputName} 
                                                value={roleName.value}
                                                placeholder={roleDetail.name ? roleDetail.name : ''}
                                                options={{
                                                    maxLength: 10,
                                                    isShowError: roleName.isShowError,
                                                    errorMsg: roleName.errorMsg,
                                                }}
                                             />
                                            
                                         </div>) 
                                       : ( roleDetail.name ? <span className="modal-col-input-content">{roleDetail.name}</span> : '--'),
                    'hasFooter': true,
                    "hasInput" : isEdit
                },
                {
                    "title"  : 'Description',
                    "content":  isEdit ? (<div className="modal-col-input-wapper">
                                            <VodaInput className="add-device-input" 
                                                onChange={this.handleInputDescription} 
                                                value={roleDetail.description}
                                                options={{
                                                    maxLength: 100,
                                                }}
                                            />
                                         </div>) 
                                       : ( roleDetail.description ? <span className="modal-col-input-content" >{roleDetail.description}</span> : '--'),
                    'hasFooter': true,
                    "hasInput" : isEdit
                }
            ];
        {
            roleDetail && roleDetail.pages && roleDetail.pages.map( (item, index ) =>
                dData.push({
                        "title"  : item.name,
                        "content": (<div className="modal-col-select-wapper">
                            <SelectBox className={"add-device-input"} 
                                        onChange={value => this.handleSelect(value, item.id)}
                                        allowClear = {false}
                                        value={item.operationID}
                                        option={this.roleOption}
                                        disabled={!isEdit}
                            /></div>),
                        'hasFooter': true,
                        "hasInput" : isEdit
                    })
                );
        }
            
            
        return(
            <div>
                <div className="modal-model-detail-wapper">
                    <InfoBox data={hData}/>
                    <div className="vfsm-add-new-role-title">
                        <span>Set role for page: </span>
                    </div>
                </div>
                <div className="vfsm-detail-role-content">
                    <div className="vfsm-detail-role-data">
                         <InfoBox data={dData} />
                    </div>
                </div>
                {
                    menuRoles&&menuRoles.rolePolicy == 'operation.write' ?
                    (
                        <ButtonFooter text={ isEdit ? "Save" : "Edit" } 
                        id={isEdit ? 'vfsm-button-footer-modal-save' : 'vfsm-button-footer-modal-edit'}
                        className="model-detail-button"
                        disabled={this.state.disabled || isAdmin} 
                        loading={loading}
                        handleClickCancel={this.handleCancel} 
                        handleClickFirst={this.handleClickFirst}/> 
                    ): null
                }
            </div>
        )
    }
}

Detail.propTypes = {
    id: PropTypes.string.isRequired,
}

export default Detail;

