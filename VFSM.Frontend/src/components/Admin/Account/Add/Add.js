
import React from 'react';
import { inject, observer } from 'mobx-react';
import { toJS } from 'mobx';
import InfoBox from 'common/InfoBox/InfoBox';
import ButtonFooter from 'common/ButtonFooter/ButtonFooter';
import VodaInput from 'common/Input/VodaInput';
import SelectBox from 'common/Select/Select';
import Notification   from '../../../../core/ui/Notification/Notification';
import { validateEmail, checkInputExit, compareTwoInputs} from '../../../../core/utils/validate';
import Constant from  '../../../../core/utils/Constant';
import './styles.scss';

const { onOpenNotification } = Notification;
const { USER_ERROR, API_STATUS} = Constant;

@inject('rootStore')
@inject('apiStore')
@observer

class Add extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            disabled: true,
            emailBox: {
                value: '',
                isShowError: false,
                errorMsg: '',
            },
            confirmEmailBox:{
                value: '',
                isShowError: false,
                errorMsg: '',
            },
            userRole: {
                value: '',
                isShowError: false,
                errorMsg: '',
            },
            loading: false
        }
    }
    componentDidMount (){
        this.props.rootStore.account.getAllRole();
    }

    /**/
    handleCheckInput = (key) => {
        const { emailBox, confirmEmailBox, userRole } = this.state;
        const { newAccount } = this.props.rootStore.account;
        if( key === 1 && emailBox.value === ''){
            this.setState({
                emailBox: {
                    ...this.state.emailBox,
                    isShowError: true,
                    errorMsg: USER_ERROR.REQUIRED,
                },
            })
        }
        if( key === 2 && confirmEmailBox.value === ''){
            this.setState({
                 confirmEmailBox: {
                    ...this.state.confirmEmailBox,
                    isShowError: true,
                    errorMsg: USER_ERROR.REQUIRED,
                }  
            })
        }

        if( key === 3 && userRole.value === undefined){
            this.setState({
                userRole: {
                   ...this.state.userRole,
                   isShowError: true,
                   errorMsg: USER_ERROR.REQUIRED,
               }  
           })
        }
        ( checkInputExit(emailBox.value ) && checkInputExit(confirmEmailBox.value ) && checkInputExit(userRole.value 
         && !emailBox.isShowError && !confirmEmailBox.isShowError && !userRole.isShowError)) 
            ? (this.setState({ disabled: false }))
            : (this.setState({
                disabled: true,
            }))
    }

    handleInputEmail = (e) => {
        this.setState({
            emailBox : {
                ...this.state.emailBox,
                isShowError: false,
                errorMsg: '',
                value: e.target.value,
            }
        }, () => this.handleCheckInput(1));
        //this.props.rootStore.account.setEmailNewAccount(e.target.value);
    }

    handleInputEmailConfirm = (e) => {
        this.setState({
            confirmEmailBox : {
                ...this.state.confirmEmailBox,
                isShowError: false,
                errorMsg: '',
                value: e.target.value,
            }
        },() => this.handleCheckInput(2));
        //this.props.rootStore.account.setEmailConfirmNewAccount(e.target.value);
    }
    /**/

    handleCancel = () => {
        this.setState({
            isShowError: false,
            disabled : true,
        }, () => this.props.handleCancelCallBack());
    }

    handleAddCallBack = () => {
        const res = this.props.rootStore.account.response ? this.props.rootStore.account.response : '';
        const { emailBox } = this.state;
        let msgNoti = '';
        let addStatus='error';
        if(res && res.status && res.status.toUpperCase() === API_STATUS.SUCCESS) {
            this.setState({
                disabled : true
            }, () => this.handleCancel());
            msgNoti = 'Add Successfull';
            addStatus = API_STATUS.SUCCESS;
        } else {
            this.setState({
                emailBox:{
                    ...emailBox,
                    isShowError: true,
                    errorMsg: (typeof res.status === 'string') ? res.status : USER_ERROR.INVALID,
                },
                loading: false,
                disabled: true,
            });
            msgNoti = 'Add Unsuccessfull';
            addStatus = API_STATUS.ERROR;
        }
        this.openNotification(msgNoti, addStatus);
      
    }

    handleClickFirst = () => {
        const { emailBox, confirmEmailBox, userRole} = this.state;
        let bvalid=  false;
        if( !validateEmail(emailBox.value)){
            this.setState({
                disabled: true,
                emailBox:{
                    ...emailBox,
                    isShowError: true,
                    errorMsg: USER_ERROR.INCORRECT_FORMAT,
                }
            })
        }
        if(!validateEmail(confirmEmailBox.value)){
            this.setState({
                disabled: true,
                confirmEmailBox:{
                    ...confirmEmailBox,
                    isShowError: true,
                    errorMsg: USER_ERROR.INCORRECT_FORMAT,
                }
            })
        }

        if (!compareTwoInputs(emailBox.value, confirmEmailBox.value)){
            this.setState({
                confirmEmailBox: {
                   ...this.state.confirmEmailBox,
                   isShowError: true,
                   errorMsg: USER_ERROR.NOT_SAME,
               }  
           })
        }

        bvalid = validateEmail(emailBox.value) && validateEmail(confirmEmailBox.value) && compareTwoInputs(emailBox.value, confirmEmailBox.value);
        if(bvalid){
            this.props.rootStore.account.setEmailNewAccount(emailBox.value);
            this.props.rootStore.account.setEmailConfirmNewAccount(confirmEmailBox.value);
            this.props.rootStore.account.setRoleNewAccount(userRole.value);
            this.props.rootStore.account.addNew(this.handleAddCallBack);
        }
    }

    openNotification = (message, status) => {
        onOpenNotification(message,status);
    }

    handleSelectRole = (value) => {
        this.setState({
            disabled: false,
            userRole:{
                ...this.state.role,
                value: value,
                isShowError: false,
                errorMsg: '',
            }
        }, () => this.handleCheckInput (3));
       // this.props.rootStore.account.setRoleNewAccount(value);
    }

    resetState = () => {
        const { emailBox, confirmEmailBox, userRole } = this.state;
        this.setState({
            emailBox: {
                ...emailBox,
                value: '',
                isShowError: false,
                errorMsg: '',
            },
            confirmEmailBox: {
                ...confirmEmailBox,
                value: '',
                isShowError: false,
                errorMsg: '',
            },
            userRole: {
                ...userRole,
                value: '',
                isShowError: false,
                errorMsg: '',
            }
        })
    }

    // notification
    render(){
        const { confirmEmailBox, emailBox, userRole} = this.state;
        const { loading } = this.props.apiStore;
        const { newAccount } = this.props.rootStore.account;
        const roles = toJS(this.props.rootStore.account.roles);
        const data = [
            {
                "title": "Email",
                "content" :  <VodaInput className="add-device-input" 
                                placeholder="Email" 
                                onChange={this.handleInputEmail}
                                value={emailBox.value}
                                options={{
                                    maxLength: 50,
                                    isShowError: emailBox.isShowError,
                                    errorMsg: emailBox.errorMsg,
                                }}
                             />,
                "hasInput" : true
            },
            {      
                "title": "Confirm Email",
                "content" :  <VodaInput 
                                className="add-device-input" 
                                placeholder="Confirm Email" 
                                onChange={this.handleInputEmailConfirm} 
                                value={confirmEmailBox.value}
                                options={{
                                    maxLength: 50,
                                    isShowError: confirmEmailBox.isShowError,
                                    errorMsg: confirmEmailBox.errorMsg,
                                }}
                              />,
                "hasInput" : true
            },
            {
                "title": "Role",
                "content" :
                            <SelectBox className={userRole.isShowError ? 'add-device-input-error' : "add-device-input"} 
                                onChange={this.handleSelectRole} 
                                value={userRole.value}
                                option={roles}
                                options={{
                                    isShowError: userRole.isShowError ,
                                    errorMsg: userRole.errorMsg,
                                }}
                            />,
                "hasInput" : true
            }
        ];
        return(
            <div className="modal-add-user-wapper">
                <InfoBox data={data} />
                <ButtonFooter 
                    text="Create"
                    id="vfsm-add-new-user"
                    className="model-add-button"
                    disabled={this.state.disabled}
                    loading={loading}
                    handleClickCancel={this.handleCancel}
                    handleClickFirst={this.handleClickFirst}
                />
            </div>
        )
    }
}

export default  Add;

