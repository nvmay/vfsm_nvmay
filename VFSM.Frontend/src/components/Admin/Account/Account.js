import React from 'react';
import { inject, observer } from 'mobx-react';
import { toJS } from "mobx";
import { Modal, Input, Col, Row, notification } from 'antd';
import moment from 'moment';
import Card from 'common/Card/Card';
import VodaButton from 'common/VodaButton/VodaButton';
import DatePicker from 'common/DatePicker/DatePicker';
import Select from 'common/Select/Select';
import userAvatar from 'common/icon/svg/User.svg';
import Add from './Add/Add';
import AddCard from 'common/AddCard/AddCard';
import ConfirmModal from 'common/ConfirmModal/ConfirmModal';
import IconSvg from 'common/icon/IconSvg';
import Detail from './Detail/Detail';
import Constant from '../../../../src/core/utils/Constant';
import { DateTime } from 'core/utils';
import Notification from '../../../../src/core/ui/Notification/Notification';
import { checkInputExit } from '../../../../src/core/utils/validate';
import VodaBody from 'common/VodaBody/VodaBody';
import './styles.scss';

const { formatTime, formatTimeToSend } = DateTime;
const { onOpenNotification } = Notification;
const { API_STATUS } = Constant;
const pageSize = 100;
@inject('rootStore')
@inject('apiStore')
@inject('menuRoleStore')
@observer
class Account extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpenDetail: false,
            isOpenAdd: false,
            idOTA: null,
            isopenOTA: false,
            isDisabledAdd: false,
            isDisabledSearch: true,
            openModelDetail: false,
            idDetail: null,
            searchInput: '',
            sortType: 'Email',
            orderType: true,
            detailId: null,
            openConfirm: false,
            disableOrderSort: true
        }
        this.optionSort = [
            {
                label: 'User Name',
                value: 'USER_NAME'
            },
            {
                label: 'Email',
                value: 'EMAIL'
            },
        ];
    }

    componentDidMount() {
        const { sortType, orderType } = this.state;
        const param = {
            CurrentPage: 1,
            PageSize: pageSize,
            sort: sortType,
            sortAsc: orderType
        };
        this.props.rootStore.account.search(param);
    }

    /*  CallBack funcs*/
    //open OTA page callback
    handleClickOpenDetail = (id) => {
        this.props.rootStore.account.getDetail(id);
        this.setState({
            isOpenDetail: true,
            detailId: id
        })
    }
    handleClickDeleteCard = (id) => {
        this.setState({
            openConfirm: true,
            detailId: id
        })
    }
    /* End callback function */

    handleBackToModelsCallBack = () => {
        this.setState({ isopenOTA: false })
    }

    handleCancelCallBack = () => {
        this.setState({
            isOpenAdd: false,
            isDisabledAdd: false,
        })
    }

    handleCloseModelModal = () => {
        this.setState({ openModelDetail: false })
    }

    handleChangeStartDate = (value) => {
        this.setState({
            startDate: value
        });
    }

    handleChangeEndDate = (value) => {
        this.setState({
            endDateDate: value
        });
    }
    // Sorting
    handleSort = (value) => {
        if (!value) {
            this.setState({ sortType: value, orderType: undefined }, () => this.handleSearch());
        } else {
            this.setState({ sortType: value }, () => this.handleSearch());
        }
    }

    handleSortOrder = (type) => {
        this.setState({ orderType: type }, () => this.handleSearch());
    }

    checkInputSeach = () => {
        const { searchInput } = this.state;
        if (checkInputExit(searchInput)) {
            this.setState({
                isDisabledSearch: false,
            })
        } else {
            this.setState({
                isDisabledSearch: true,
            })
        }
    }

    handleInputOnChange = (value) => {
        this.setState({
            searchInput: value.target.value
        }, () => this.checkInputSeach());
    }

    handleOnPressEnter = (value) => {
        this.setState({
            searchInput: value.target.value
        }, () => this.handleSearch());
    }

    handleSearch = () => {
        const { sortType, orderType, searchInput } = this.state;
        const param = {
            CurrentPage: 1,
            PageSize: pageSize,
            sort: sortType,
            sortAsc: orderType,
            email: searchInput
        };
        this.props.rootStore.account.search(param);
    }

    /* Sync after adding */
    handleSyncCallBack = () => {
        //this.handleSearch();
    }

    handleCloseAdd = () => {
        this.setState({
            isOpenAdd: false,
            isDisabledAdd: false,
        });
    }

    handleAddModel = () => {
        this.setState({
            isOpenAdd: true,
            isDisabledAdd: true,
        })
    }
    handleCloseDetail = () => {
        this.setState({
            isOpenDetail: false
        });
    }
    // Confirm modal
    handleConfirmDelete = () => {
        const { detailId } = this.state;
        this.props.rootStore.account.remove(detailId, this.handleCloseConfirmModal);
    }

    openNotification = (message, status) => {
        onOpenNotification(message, status);
    }

    handleCloseConfirmModal = (status) => {
        let msgNoti = '';
        let deleteStatus = '';
        if (status && status.toString().toUpperCase() === API_STATUS.SUCCESS) {
            msgNoti = 'Delete Successful';
            deleteStatus = API_STATUS.SUCCESS;
            this.openNotification(msgNoti, deleteStatus);
        } else if (status && status.toString().toUpperCase() === API_STATUS.ERROR) {
            msgNoti = 'Delete Unsuccessful';
            deleteStatus = API_STATUS.ERROR;
            this.openNotification(msgNoti, deleteStatus);
        }
        this.setState({
            openConfirm: false
        });
    }

    renderAddNew = () => {
        const { isOpenAdd } = this.state;
        if (isOpenAdd) {
            return (
                <Modal
                    width={693}
                    footer={null}
                    title="Create New User"
                    visible={isOpenAdd}
                    onCancel={this.handleCloseAdd}
                >
                    <Add
                        handleCancelCallBack={this.handleCancelCallBack}
                    />
                </Modal>
            )
        }
        return null;
    }

    renderDetailUser = () => {
        const { isOpenDetail } = this.state;
        if (isOpenDetail) {
            return (
                <Modal
                    width={693}
                    footer={null}
                    title="User Detail"
                    visible={isOpenDetail}
                    onCancel={this.handleCloseDetail}
                >
                    <Detail handleClickCancelCallback={this.handleCloseDetail} />
                </Modal>
            )
        }
        return null;
    }

    render() {
        const { loading } = this.props.apiStore;
        const { openConfirm, detailId, isDisabledAdd, isDisabledSearch } = this.state;
        const items = this.props.rootStore.account.accounts;
        const { menuRoles } = this.props.menuRoleStore;
        const dataFiltered = items ? items : [];

        const selectOption = [
            {
                "title": "Detail",
                "className": "select-option"
            },
            {
                "title": "Remove",
                "className": "select-option-delete"
            }

        ]
        const body = <Row gutter={24} style={{ paddingBottom: 20 }}>
            {
                dataFiltered && dataFiltered.length > 0
                    ? (
                        <div className="vfsm-user-body-container">
                            {
                                dataFiltered.map((prop, index) =>
                                    <Col className="gutter-row" span={6}  >
                                        <Card
                                            data={prop}
                                            Id={prop.id}
                                            key={index}
                                            selectOption={selectOption}
                                            handleClickOpenDetail={this.handleClickOpenDetail}
                                            handleClickDeleteCard={this.handleClickDeleteCard}
                                            handleClickMenuCallBack={this.handleClickMenuCallBack}
                                            avatar={userAvatar}
                                            menuRole={menuRoles.user}
                                        />
                                    </Col>
                                )
                            }
                        </div>
                    )
                    :
                    (
                        loading ? <div></div> : <div style={{ fontSize: '30px', fontWeight: 500, textAlign: 'center', padding: '50px', fontStyle: 'italic' }}>No found the result</div>
                    )
            }
        </Row>
        return (
            <div className="vfsm-user-container">
                <Row className="vfsm-user-header">
                    <Col span={12} className='vfsm-create-new-user'>
                        {
                            menuRoles && menuRoles.user == 'operation.write' ? (
                                <div className="vfsm-create-new-user-button">
                                    <VodaButton text="Create New" iconType="Plus" onClick={this.handleAddModel} disabled={isDisabledAdd} />
                                </div>) : null
                        }
                    </Col>
                    <Col span={12} className="row-end">
                        {/* <Select className="ml-1" placeholder="Type: Search type" option={optionSearch} onChange={this.handleChangeSearchType} /> */}
                        <Input style={{ marginLeft: '15px', width: '271px' }}
                            placeholder={"Search by Email"}
                            onPressEnter={this.handleOnPressEnter}
                            onChange={this.handleInputOnChange} />
                        <VodaButton text="Search" iconType="Search" onClick={this.handleSearch} />
                    </Col>
                </Row>
                <div className="vfsm-user-body">
                    <VodaBody
                        optionSort={this.optionSort}
                        onChangeSort={this.handleSort}
                        body={body}
                        onSortOrder={this.handleSortOrder}
                    />
                </div>
                {this.renderDetailUser()}
                {this.renderAddNew()}
                <ConfirmModal
                    visible={openConfirm}
                    title='Are you sure you want to remove this User?'
                    handleConfirm={this.handleConfirmDelete}
                    handleCloseModal={this.handleCloseConfirmModal}
                />
            </div>
        )
    }
}

export default Account;