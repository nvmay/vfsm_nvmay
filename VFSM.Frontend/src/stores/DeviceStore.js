import { observable, configure, action } from 'mobx';
import BaseStore from '../core/stores/BaseStore';
import { DeviceService } from '../services'
// import { action, runInAction } from 'mobx';

// don't allow state modifications outside actions
configure({ enforceActions: 'observed' });

export default class DeviceStore extends BaseStore {
    @observable devices = [];
    @observable deviceDetail = {};

    //Get detail Device
    @action
    async getDetail(deviceId) {
        this.deviceDetail = {};
        const jobRequest = () => { return DeviceService.getDetail(deviceId); };
		this.call(jobRequest,
			response => {
                this.deviceDetail = response.response;
			}
		);
    }

    
    //Edit device name
    @action
    async editDeviceName(body, callback) {
        const jobRequest = () => { return DeviceService.editDeviceName(body); };
        this.call(jobRequest,
            response => {
                if (callback) {
                    callback();
                }
                this.deviceDetail.deviceName = response.response.name;
            }
        );
    }
}