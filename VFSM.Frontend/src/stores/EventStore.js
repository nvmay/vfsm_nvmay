import { observable, configure, action } from 'mobx';
import BaseStore from '../core/stores/BaseStore';
import { EventService } from '../services';
// import { action, runInAction } from 'mobx';

// don't allow state modifications outside actions
configure({ enforceActions: 'observed' });

export default class EventStore extends BaseStore {
    @observable events = [];
    @observable eventDetail = [];
    @observable lastEventDetail = {};

    //Get list Customer
    @action
    async getAll(body) {
        let jobRequest = null;
        jobRequest = () => { return EventService.getAll(body); };
        this.call(jobRequest,
            response => {
                this.events = response.response;
            }
        );
    }

    //Get detail Event
    @action
    async getDetail(deviceId, body) {
        this.eventDetail = [];
        const jobRequest = () => { return EventService.getDetail(deviceId, body); };
        this.call(jobRequest,
            response => {
                this.eventDetail = response.response;
            }
        );
    }

    //Get detail Last act
    @action
    async getLastActDetail(deviceId) {
        this.lastEventDetail = {};
        const jobRequest = () => { return EventService.getLastActDetail(deviceId); };
        this.call(jobRequest,
            response => {
                this.lastEventDetail = response.response;
            }
        );
    }
}