import { observable, configure, action } from 'mobx';
import BaseStore from '../core/stores/BaseStore';
import { UserService } from '../services'
import { exportFileExcel } from '../core/utils/exportFile';
// import { action, runInAction } from 'mobx';

// don't allow state modifications outside actions
configure({ enforceActions: 'observed' });

export default class UserStore extends BaseStore {
    @observable users = [];
    @observable userDetail = {};
    @observable exportBody = {};

    //Get list Customer
    @action
    async getAll(body) {
        let jobRequest = null;
        this.exportBody = body;
        jobRequest = () => { return UserService.getAll(body); };
        this.call(jobRequest,
            response => {
                this.users = response.response;
            }
        );
    }

    //Get detail Customer
    @action
    async getDetail(userId) {
        this.userDetail = {};
        const jobRequest = () => { return UserService.getDetail(userId); };
        this.call(jobRequest,
            response => {
                this.userDetail = response.response;
            }
        );
    }

    //Add new Device
    @action
    async addNewDevice(body, callback) {
        const jobRequest = () => { return UserService.addNewDevice(body); };
        this.call(jobRequest,
            response => {
                if (callback) {
                    callback();
                }

                return this.users.items.map(item => {
                    if (item.userId === response.response.userId) {
                        response.response.role === "SUB"
                        ? item.device = [...item.device, response.response]
                        : item.device = [response.response, ...item.device];
                    }
                    return item;
                });
            }
        );
    }

    //Edit nick name
    @action
    async editNickName(body, callback) {
        const jobRequest = () => { return UserService.editNickName(body); };
        this.call(jobRequest,
            response => {
                if (callback) {
                    callback();
                }
                this.userDetail.nickName = response.response.nickName;
                return this.users.items.map(item => {
                    if (item.userId === response.response.userId) {
                        item.nickName = response.response.nickName;
                    }
                    return item;
                });
            }
        );
    }

    //Get detail Customer
    @action
    async exportUser() {
        const jobRequest = () => { return UserService.exportUser(this.exportBody); };
        this.call(jobRequest,
            response => {
                if (response.response) {
                    exportFileExcel(response.response);
                }
            }
        );
    }
}