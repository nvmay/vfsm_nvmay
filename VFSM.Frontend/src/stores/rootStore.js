
import UserStore from './UserStore';
import DeviceStore from './DeviceStore';
import ModelStore from './ModelStore';
import OTAStore from './OTAStore';
import EventStore from './EventStore';
import AccountStore from './AccountStore';
import RoleStore from './RoleStore';
import SeriesStore from './SeriesStore'

export default class RootStore {

    constructor() {
        this.user = new UserStore(this);
        this.device = new DeviceStore(this);
        this.model = new ModelStore(this);
        this.ota = new OTAStore(this);
        this.event = new EventStore(this);
        this.account = new AccountStore(this);
        this.role = new RoleStore(this);
        this.series = new SeriesStore(this);
    }
}
