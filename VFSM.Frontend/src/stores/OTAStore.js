import { observable, configure, action, runInAction } from 'mobx';
import BaseStore from '../core/stores/BaseStore';
import { fromJS } from 'immutable';
import { OTAService } from '../services';
import { DateTime } from 'core/utils';
import { toJS } from "mobx";

// don't allow state modifications outside actions
configure({ enforceActions: 'observed' });

const { formatTime, formatTimeDetail } = DateTime;
export default class OTAStore extends BaseStore {
    @observable mc60 = [];
    @observable bracelet = [];
    @observable details = {};
    @observable modelId = '';

    // get all model
    @action
    getByModelId(modelId) {
        const jobRequest = () => { return OTAService.getByModelId(modelId); };
        this.call(jobRequest,
            response => {
                this.divideOta(response.response);
            }
        );
    }
    // Create OTA
    @action
    createOta(param, callback) {
        const jobRequest = () => { return OTAService.createOta(param); }
        this.call(jobRequest,
            response => {
                const res = response.response;
                if (res.status === "Success") {
                    res.data.id = res.data.version;
                    res.data.modifiedDate = formatTime(res.data.modifiedDate);
                    res.data.notEdit = ['fileSize', 'modifiedDate'];
                    // Notify here
                    let mc60Array = toJS(this.mc60);
                    let braceletArray = toJS(this.bracelet);
                    switch (res.data.category) {
                        case 'MC60':
                            this.mc60.shift();
                            this.mc60.unshift(res.data);
                            break;
                        case 'FIRMWARE':
                            this.bracelet.shift();
                            this.bracelet.unshift(res.data);
                            break;
                        default:
                            break;
                    }
                    if (callback) {
                        callback(res.status, 'Add Successful!');
                    }
                } else {
                    // Notify and remove new record in table
                    if (callback) {
                        callback(res.status, 'Add Fail! ' + res.data);
                    }
                }
            },
            error => {
                if (callback) {
                    callback('error', 'Add Fail!');
                }
            }
        );
    }
    // Edit OTA
    @action
    editOta(param, callback, category) {
        const jobRequest = () => { return OTAService.editOta(param); }
        this.call(jobRequest,
            response => {
                const res = response.response;
                if (res.status === "Success") {
                    // Notify here
                    res.data.id = res.data.version;
                    res.data.modifiedDate = formatTime(res.data.modifiedDate);
                    res.data.notEdit = ['fileSize', 'modifiedDate'];
                    this.updateSuccess(category, res.data);
                    if (callback) {
                        callback(res.status, 'Update Successful!');
                    }
                } else {
                    // Notify and remove new record in table
                    if (callback) {
                        callback(res.status, 'Update Fail! ' + res.data);
                    }
                }
            },
            error => {
                if (callback) {
                    callback('error', 'Update Fail!');
                }
            }
        );
    }
    // Delete OTA list
    @action
    deleteOtaList(deleteList, callback) {
        const jobRequest = () => { return OTAService.deleteOtaList(deleteList); }
        this.call(jobRequest,
            response => {
                const res = response.response.data;
                res.listSuccess.forEach(item => {
                    if (item.category === 'MC60') {
                        this.mc60 = this.mc60.filter(o => o.id !== item.version);
                    }
                    if (item.category === 'FIRMWARE') {
                        this.bracelet = this.bracelet.filter(o => o.id !== item.version);
                    }
                });
                // Notification here
                if (callback) {
                    callback(response.response.status);
                }
            },
            error => {
                if (callback) {
                    callback('error');
                }
            }

        );
    }
    @action
    resetData() {
        this.mc60 = [];
        this.bracelet = [];
    }
    @action
    divideOta(data) {
        let mc60Array = [];
        let braceletArray = [];
        data.forEach(item => {
            item.modifiedDate = item.modifiedDate ? formatTimeDetail(item.modifiedDate) : formatTimeDetail(item.createdDate);
            if (item.category.toUpperCase() == 'MC60') {
                item.id = item.version;
                item.notEdit = ['fileSize', 'modifiedDate'];
                mc60Array.push(item);
            }
            if (item.category.toUpperCase() == 'FIRMWARE') {
                item.id = item.version;
                item.notEdit = ['fileSize', 'modifiedDate'];
                braceletArray.push(item);
            }
        }); 
        this.mc60 = [...mc60Array];
        this.bracelet = [...braceletArray];
    }
    @action
    setModelId(modelId) {
        this.modelId = modelId;
    }
    @action
    setListData(data, category) {
        category === 'MC60' ? this.mc60 = [...data] : this.bracelet = [...data];
    }
    @action
    cancelAdd(category) {
        if (category === 'MC60') {
            this.mc60.forEach((element, index, object) => {
                if (element.id === 0) {
                    object.splice(index, 1);
                }
            })
        } else {
            this.bracelet.forEach((element, index, object) => {
                if (element.id === 0) {
                    object.splice(index, 1);
                }
            })
        }
    }
    @action
    setFileSize(category, idRow, fileSize) {
        if (category === 'MC60') {
            this.mc60.forEach(element => {
                if (element.id === idRow) {
                    element.fileSize = fileSize;
                }
            })
        } else {
            this.bracelet.forEach(element => {
                if (element.id === idRow) {
                    element.fileSize = fileSize;
                }
            })
        }
    }
    @action
    updateSuccess(category, data) {
        let mc60Array = toJS(this.mc60);
        let braceletArray = toJS(this.bracelet);
        if (category === 'MC60') {
            mc60Array.forEach((item, index, array) => {
                if (item.version === data.version) {
                    array.splice(index, 1);
                }
            });
            mc60Array.unshift(data);
        } else {
            braceletArray.forEach((item, index, array) => {
                if (item.version === data.version) {
                    array.splice(index, 1);
                }
            });
            braceletArray.unshift(data);
        }
        this.mc60 = mc60Array.slice();
        this.bracelet = braceletArray.slice();
    }
}