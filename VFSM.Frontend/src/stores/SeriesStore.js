import { observable, configure, action } from 'mobx';
import BaseStore from '../core/stores/BaseStore';
import { SeriesService } from '../services';
// import { action, runInAction } from 'mobx';

// don't allow state modifications outside actions
configure({ enforceActions: 'observed' });

export default class SeriesStore extends BaseStore {
    @observable seriesData = [];

    //Get list Series
    @action
    async getSeries(activityId) {
        let jobRequest = null;
        jobRequest = () => { return SeriesService.getSeries(activityId); };
        this.call(jobRequest,
            response => {
                this.seriesData = response.response;
            }
        );
    }

}