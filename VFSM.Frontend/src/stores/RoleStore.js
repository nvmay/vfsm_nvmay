import { observable, configure, action } from 'mobx';
import BaseStore from '../core/stores/BaseStore';
import { RoleService } from '../services'
// import { action, runInAction } from 'mobx';

// don't allow state modifications outside actions
configure({ enforceActions: 'observed' });

export default class RoleStore extends BaseStore {
    @observable roles = [];
    @observable roleDetail = {};
    @observable addStatus = '';

    //Search
    @action
    async search(param) {
        let jobRequest = null;
        jobRequest = () => { return RoleService.search(param); };
		this.call(jobRequest,
			response => {
                this.roles = response.response;
			}
		);
    }
    //Get detail
    @action
    async getDetail(Id) {
        let jobRequest = null;
        jobRequest = () => { return RoleService.getDetail(Id); };
		this.call(jobRequest,
			response => {
                this.roleDetail = response.response;
			}
		);
    }
    // add new role
    @action
    async addNew(body, callBack) {
        const jobRequest = () => {return RoleService.addNew(body)}
        this.call(jobRequest,
            response => {
                // Get response
                var res = response.response;
               if(res.status.toUpperCase() === 'SUCCESS'){
                    this.roles.unshift(res.data);
                    this.addStatus = res.status;
               } else {
                    this.addStatus = 'exited';
               }
              
               if(callBack) {
                 callBack();
               }
            },
            error => {
                this.addStatus = 'exited';
                if(callBack) {
                    callBack();
                  }
            }
        );

    }

    // remove role
    @action
    async remove(body, callBack) {
        const jobRequest = () => {return RoleService.remove(body)}
        this.call(jobRequest,
            response => {
                if(response.response.toUpperCase() === 'SUCCESS'){
                    const newRoles = this.roles;
                    newRoles.splice(newRoles.findIndex(item => item.id === body), 1);
                    this.roles = [...newRoles];
                    if(callBack){
                        callBack('SUCCESS');
                    }
                }
              
            },
            error => {
                if(callBack){
                    callBack('error');
                }
            }
        );

    }
    // Edit role
    @action
    async editRole(callBack) {
        var id = this.roleDetail.id;
        const jobRequest = () => {return RoleService.editRole(this.roleDetail)}
        this.call(jobRequest,
            response => {
                // Get response
                var res = response.response;
               if(res.status.toUpperCase() === 'SUCCESS'){
                    this.roles.forEach(item => {
                        const index = this.roles.findIndex(item => item.id === id);
                        this.roles[index] = res.data;
                    });
                    if(callBack) {
                        callBack('SUCCESS');
                    }
               } else if (res.status.toUpperCase() === 'EXISTED'){
                    if(callBack) {
                        callBack('EXITED');
                    }
               }
              
            },
            error =>{ 
                if(callBack) {
                callBack('ERROR');
                }
            }
        );

    }
    @action
    async setOperation(value, id) {
        if (this.roleDetail) {
            this.roleDetail.pages.forEach(element => {
                if (element.id == id) {
                    element.operationID = value;
                }
            });
        }

    }
    @action
    async setName(value) {
        if (this.roleDetail) {
            this.roleDetail.name = value;
        }
    }
    @action
    async setDescription(value) {
        if (this.roleDetail) {
            this.roleDetail.description = value;
        }
    }

    @action
    async  filterDataRender (searchInput){
        let newArr = (this.roles);
        if(searchInput !== '' && newArr.length > 0 ){
            newArr = newArr.filter(item => item.name.toLowerCase().search(searchInput.toLowerCase()) !== -1);
            this.roles = newArr;
        } else {
            this.search('');
        }
    }
}