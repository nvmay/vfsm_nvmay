import { observable, configure, action, toJS } from 'mobx';
import BaseStore from '../core/stores/BaseStore';
import { AccountService, RoleService } from '../services';
import { fromJS } from 'immutable';
import Cookies from 'js-cookie';
import routes from 'routes';
import MenuRoleStore from 'core/stores/MenuRoleStore';
// import { action, runInAction } from 'mobx';

// don't allow state modifications outside actions
configure({ enforceActions: 'observed' });

const userDefault = fromJS({});
export default class AccountStore extends BaseStore {
    @observable accounts = [];
    @observable accountDetail = {};
    @observable newAccount = {};
    @observable response = {};
    @observable roles = [];
    @observable currentUser = userDefault;
    @observable menuRole = routes;

    @action
    signIn(payload, callBack ) {
        const jobRequest =  () => { return  AccountService.signIn(payload); };
        this.call(jobRequest, 
            response => {
                if(response.response.token){
                    if(payload.RememberMe){
                        var exhours = 1;
                        var d = new Date();
                        d.setTime(d.getTime() + (exhours*60*60*1000));
                        var expires = d;
                        Cookies.set('token', response.response.token, { path: '/' , expires: expires});
                    }
                    else {
                        Cookies.set('token', response.response.token, { path: '/' });
                    }

                    if(callBack){
                        callBack('success');
                    }
                }
                else if (callBack){
                    callBack(response.response.response);
                }
            },
            error => {
                if(callBack){
                    callBack('error');
                }
            }
        );
 
    }

    @action
    ResetPassword(payload, callBack ) {
        const jobRequest =  () => { return  AccountService.ResetPassword(payload); };
        this.call(jobRequest, 
            response => {
                if(response.response){
                    if(callBack && response.response.status){
                        callBack(response.response.status);
                    }
                }
            }
        );
 
    }

    @action
    forgotPassword(payload, callBack ) {
        const jobRequest =  () => { return  AccountService.forgotPassword(payload); };
        this.call(jobRequest, 
            response => {
                if(response.response){
                    if(callBack && response.response.status){
                        callBack(response.response.status);
                    }
                }
            }
        );
 
    }

    @action
    getCurrentUser() {
        this.currentUser = userDefault;
        let jobRequest = null;
        jobRequest = () => { return AccountService.getCurrentUser(); };
        this.call(jobRequest,
            response => {
                if (response.response) {
                    this.currentUser = response.response;
                    // Set role
                    if (this.currentUser && this.currentUser.roles) {
                        toJS(this.currentUser.roles.pages).forEach(item => {
                            // Set role for menu
                            MenuRoleStore.setMenuRoles(item.pageValue, item.operationValue);
                        });
                    }
                    
                } else if (response.error.response.status === 401 && response.error.response.statusText === 'Unauthorized') {
                    Cookies.remove('token');
                }
            }
        );
    }
    //Search
    @action
    search(param) {
        let jobRequest = null;
        jobRequest = () => { return AccountService.search(param); };
        this.call(jobRequest,
            response => {
                this.accounts = response.response.items;
                this.accounts.forEach(item => {
                    item.name = item.email;
                });
            }
        );
    }
    //Get detail
    @action
    getDetail(Id) {
        let jobRequest = null;
        jobRequest = () => { return AccountService.getDetail(Id); };
        this.call(jobRequest,
            response => {
                this.accountDetail = response.response;
            }
        );
    }
    //Add new 
    @action
    addNew(callBack) {
        const jobRequest = () => { return AccountService.addNew(this.newAccount) }
        this.call(jobRequest,
            response => {
                // Get response
                this.response = response.response;
                let data = this.response.data;
                if(data && typeof data === 'object'){
                    data.name = data.email;
                    if (this.response.status.toUpperCase() === 'SUCCESS') {
                        this.accounts.unshift(data);
                    }
                } else {
                    this.response.status = data;
                }
                if (callBack) {
                    callBack();
                }
            }
        );

    }
    // Edit accounts
    @action
    edit(callBack) {
        var id = this.accountDetail.id;
        const jobRequest = () => { return AccountService.edit(this.accountDetail) }
        this.call(jobRequest,
            response => {
                // Get response
                this.response = response.response;
                if (this.response.status.toUpperCase() === 'SUCCESS') {
                    this.accounts.forEach(item => {
                        if (item.id == id) {
                            item = this.response.data;
                        }
                    });
                }
                if (callBack) {
                    callBack();
                }
            }
        );

    }
    // Remove accounts
    @action
    remove(id, callBack) {
        const jobRequest = () => { return AccountService.remove(id) }
        this.call(jobRequest,
            response => {
                // Get response
                this.response = response.response;
                if (this.response.status.toUpperCase() === 'SUCCESS') {
                    const acc = toJS(this.accounts);
                    this.accounts = acc.filter(o => o.id !== id);
                }
                if (callBack) {
                    callBack('success');
                }
            }
        );  
    }
    // Get All Role
    @action
    getAllRole() {
        const jobRequest = () => { return RoleService.search() }
        this.call(jobRequest,
            response => {
                // Get response
                this.roles = [];
                response.response.forEach(item => {
                    let role = {};
                    role.value = item.id;
                    role.label = item.name;
                    this.roles.push(role);
                });
            },
        );

    }

    @action
    setName(value) {
        if (this.accountDetail) {
            this.accountDetail.UserName = value;
        }
    }

    @action
    setPhone(value) {
        if (this.accountDetail) {
            this.accountDetail.phoneNumber = value;
        }
    }
    @action
    setRole(value) {
        if (this.accountDetail) {
            this.accountDetail.roleId = value;
            this.accountDetail.role = (toJS(this.roles).find(o => { return o.value == value })).label;
        }
    }
    @action
    setEmailNewAccount(value) {
            this.newAccount.email = value;
    }
    @action
    setEmailConfirmNewAccount(value) {
            this.newAccount.emailConfirm = value;
    }
    @action
    setRoleNewAccount(value) {
            this.newAccount.roleId = value;
            this.newAccount.role = (toJS(this.roles).find(o => { return o.value == value })).label;
    }
}