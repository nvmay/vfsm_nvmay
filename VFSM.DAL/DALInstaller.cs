﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VFSM.DAL
{
    public class DALInstaller: Module
    {        
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(ThisAssembly)
           .Where(t => t.Name.Contains("Repository") || t.Name.Contains("Helper") || t.Name.Contains("Utility")) 
           .AsImplementedInterfaces();
        }
    }
}
