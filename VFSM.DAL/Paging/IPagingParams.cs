﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VFSM.DAL.Paging
{
    public interface IPagingParams
    {
        int CurrentPage { get; set; }
        int PageSize { get; set; }
    }
}
