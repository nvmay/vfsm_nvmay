using Newtonsoft.Json;

namespace VFSM.DAL.Models
{
    public class ResponseModel
    {
        [JsonProperty("success")]
        public bool Success { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("redirect_method")]
        public string RedirectMethod { get; set; }

        [JsonProperty("parameter_id")]
        public string ParameterId { get; set; }

        [JsonProperty("parameter_page")]
        public int ParameterPage { get; set; }
    }
}