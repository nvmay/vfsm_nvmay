namespace VFSM.DAL.Models
{
    public class ChceckboxModel
    {
        public string Value { get; set; }
        public string Text { get; set; }
        public bool Checked { get; set; }
    }
}