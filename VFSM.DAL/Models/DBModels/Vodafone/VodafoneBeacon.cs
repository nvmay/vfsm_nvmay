using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VFSM.DAL.Models.DBModels
{
    [Table("T_BEACON")]
    public class VodafoneBeacon
    {
        [Key]
        [Required]
        [StringLength(36)]
        [Column("BEACON_ID")]
        [Display(Name = "Beacon Id")]
        public string BeaconId { get; set; }

        [StringLength(36)]
        [Column("DEVICE_ID")]
        [Display(Name = "Device Id")]
        public string DeviceId { get; set; }

        [Required]
        [Column("MAC")]
        [StringLength(36)]
        [Display(Name = "MacAddress")]
        [RegularExpression(@"^[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}$")]
        public string Mac { get; set; }

        [Column("NAME")]
        [StringLength(128)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Column("LAT")]
        [Display(Name = "LAT")]
        public double Lat { get; set; }

        [Column("LNG")]
        [Display(Name = "LNG")]
        public double Lng { get; set; }

        [Column("CREATED_DATE")]
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? CreatedDate { get; set; }

        [Column("MODIFIED_DATE")]
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? ModifiedDate { get; set; }
    }
}