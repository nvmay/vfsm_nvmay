﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace VFSM.DAL.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        // TODO: 추후 국가코드 구분자 추가해야함
        //public string Country { get; set; }

        public ICollection<ApplicationUserRole> UserRoles { get; set; }
    }
}
