﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VFSM.DAL.Models
{
    [Table("T_ADMIN_USER")]
    public class VodafoneAccount
    {
        [Key]
        [Required]
        [Column("ID")]
        [StringLength(36)]
        public string Id { get; set; }
        [Required]
        [EmailAddress]
        [Column("EMAIL")]
        public string Email { get; set; }
        [Required]
        [EmailAddress]
        [Column("EMAIL_CONFIRMED")]
        public bool EmailConfirmed { get; set; }
        [Required]
        [Column("USER_NAME")]
        public string UserName { get; set; }
        [Column("PHONE_NUMBER")]
        [StringLength(24)]
        public string PhoneNumber { get; set; }
    }
}
