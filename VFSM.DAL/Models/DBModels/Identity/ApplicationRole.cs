using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace VFSM.DAL.Models
{
    public class ApplicationRole : IdentityRole
    {
        public ICollection<ApplicationUserRole> UserRoles { get; set; }

        public ApplicationRole(){}
        
        public ApplicationRole(string roleName)
        : base(roleName)
        {}
        public string Description { get; set; }
    }
}