﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VFSM.DAL.Models
{
    [Table("T_ADMIN_OPERATION")]
    public class VodafoneOperation
    {
        [Key]
        [Required]
        [Column("ID")]
        [StringLength(36)]
        public string Id { get; set; }

        [Column("NAME")]
        [StringLength(128)]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Column("VALUE")]
        [StringLength(128)]
        [Display(Name = "Value")]
        public string Value { get; set; }
    }
}
