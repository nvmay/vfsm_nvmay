﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VFSM.DAL.Models.DBModels;

namespace VFSM.DAL.Repositories
{

    public class VodafoneDeviceRepository : Repository<VodafoneDevice>, IVodafoneDeviceRepository
    {
        public VodafoneDeviceRepository(VodafoneContext context) : base(context)
        { }
        private VodafoneContext _apContext => (VodafoneContext)_context;
    }
}
