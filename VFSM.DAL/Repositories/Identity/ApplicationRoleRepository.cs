﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Models;
using VFSM.DAL.Repositories;

namespace VFSM.DAL.Repositories
{
    public class ApplicationRoleRepository : Repository<ApplicationRole>, IApplicationRoleRepository
    {
        public ApplicationRoleRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _apContext => (ApplicationDbContext)_context;
    }
}