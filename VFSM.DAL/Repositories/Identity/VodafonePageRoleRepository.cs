﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Models;

namespace VFSM.DAL.Repositories
{
    public class VodafonePageRoleRepository : Repository<VodafonePageRole>, IVodafonePageRoleRepository
    {
        public VodafonePageRoleRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _apContext => (ApplicationDbContext)_context;
    }
}
