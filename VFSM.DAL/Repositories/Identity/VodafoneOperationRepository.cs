﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Models;

namespace VFSM.DAL.Repositories
{
    public class VodafoneOperationRepository : Repository<VodafoneOperation>, IVodafoneOperationRepository
    {
        public VodafoneOperationRepository(ApplicationDbContext context) : base(context)
        { }

        private ApplicationDbContext _apContext => (ApplicationDbContext)_context;
    }
}
