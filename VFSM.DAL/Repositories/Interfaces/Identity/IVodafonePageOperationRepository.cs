﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Models;

namespace VFSM.DAL.Repositories
{
    public interface IVodafonePageOperationRepository : IRepository<VodafonePageOperation>
    {
    }
}
