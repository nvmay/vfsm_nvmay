﻿using System;
using System.Collections.Generic;
using System.Text;
using VFSM.DAL.Repositories;

namespace VFSM.DAL
{
    /// <summary>
    /// Create class Interface allow definition function, entity...
    /// </summary>
    public interface IUnitOfWork
    {
        IVodafoneUserRepository VodafoneUsers { get; }
        IVodafoneDeviceRepository VodafoneDevices { get; }
        IVodafoneBeaconsRepository VodafoneBeacons { get; }
        IVodafoneModelRepository VodafoneModel { get; }
        IVodafoneModelOTARepository VodafoneModelOTA { get; }
        IVodafoneUserDeviceRepository VodafoneUserDevice { get; }

        // Identity
        IApplicationRoleRepository Role { get; }
        IApplicationUserRepository Users { get; }
        IVodafonePageRepository VodafonePages { get; }
        IVodafoneOperationRepository VodafoneOperation { get;}
        IVodafonePageRoleRepository VodafonePageRole { get; }
        IVodafonePageOperationRepository VodafonePageOperation { get; }

        int SaveChange();
    }
}
