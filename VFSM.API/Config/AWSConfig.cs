namespace VFSM.API.Config
{
    public class AWSConfig
    {
        // AWS key ID
        public string AWS_ACCESSS_KEY_ID { get; set; }

        // AWS secret access key
        public string AWS_SECRET_ACCESS_KEY { get; set; }

        // AWS bucket
        public string AWS_BUCKET { get; set; }

        public string AWS_EMAIL_SENDER { get; set; }
        public string AWS_EMAIL_REGION { get; set; }
    }
}