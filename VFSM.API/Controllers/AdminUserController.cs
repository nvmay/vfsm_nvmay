using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using ReflectionIT.Mvc.Paging;
using Microsoft.AspNetCore.Hosting;
using VFSM.API.Data;
using VFSM.API.Models.AdminUserViewModels;
using VFSM.API.Models.RoleViewModels;
using VFSM.API.Models.AccountViewModels;
using VFSM.API.Constants;
using VFSM.API.Models;
using VFSM.Services;
using VFSM.DAL.Models;
using VFSM.DAL.Models.DBModels;

namespace VFSM.API.Controllers
{
    [Authorize(Roles = "Admin, Profile Manager")]
    public class AdminUserController : BaseController
    {
        private RoleManager<ApplicationRole> _roleManager;

        private readonly IEmailService _emailSender;

        private readonly IHostingEnvironment _hostingEnv;

        public AdminUserController(RoleManager<ApplicationRole> roleManager, 
                            UserManager<ApplicationUser> userManager, 
                            VodafoneContext context, IEmailService emailSender, 
                            IHostingEnvironment hostingEnv)
        :base(context, userManager)
        {
            _roleManager = roleManager;
            _emailSender = emailSender;
            _hostingEnv = hostingEnv;
        }

        #region Index
        [HttpGet]
        public async Task<IActionResult> Index(string filter, string sortExpression = "Email", int page = 1)
        {
            var qry = _userManager.Users.AsNoTracking().Include(u => u.UserRoles).ThenInclude(ur => ur.Role).AsQueryable();
            
            if(!String.IsNullOrEmpty(filter))
            {
                qry = qry.Where(u => u.Email.Contains(filter));
            }
            var model = await PagingList<ApplicationUser>.CreateAsync(qry, VF_CONSTANT.PageSize, page, sortExpression, "Email");
            model.RouteValue = new RouteValueDictionary { {"filter", filter} };
            return View(model);
        }
        #endregion

        #region Detail
        public async Task<IActionResult> Details(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            var user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            var convertUser = new AdminUserViewModel{
                Id = user.Id,
                UserName = user.UserName,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                PhoneNumberConfirmed = user.PhoneNumberConfirmed,
                EmailConfirmed = user.EmailConfirmed,
                Roles = await _userManager.GetRolesAsync(user)
            };

            return View(convertUser);
        }
        #endregion

        #region Edit
        public async Task<IActionResult> Edit(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            var user = await _userManager.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).SingleOrDefaultAsync(m => m.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            ViewData["IsAdminUser"] = IsAdminUser();

            if (!user.EmailConfirmed)
            {
                AddErrors(new ApplicationException("The user not confirmed email address. Please add role after email confirmation."));
                return View(new AdminUserUpdateViewModel(){ Id = string.Empty, Roles = new List<ChceckboxModel>()});
            }

            var roles = from r in _roleManager.Roles
                        select r.Name;
            
            List<ChceckboxModel> selectItems = new List<ChceckboxModel>();
            foreach(var role in roles)
            {
                selectItems.Add(new ChceckboxModel{
                    Text = role,
                    Value = role,
                    Checked = user.UserRoles.Any(r => r.Role.Name == role)
                });
            }

            var convertUser = new AdminUserUpdateViewModel{
                Id = user.Id,
                Roles = selectItems.OrderBy(x => x.Text).ToList()
            };

            
            
            return View(convertUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Roles")] AdminUserUpdateViewModel user)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var userModel = await _userManager.Users.SingleOrDefaultAsync(m => m.Id == id);

                    if (!userModel.EmailConfirmed)
                    {
                        
                    }
                    
                    IdentityResult roleUpdateResult = null;
                    
                    // 전체 삭제 후 갱신된 role만 삽입
                    // Step 1. 전체 삭제
                    var originalRoles = await _userManager.GetRolesAsync(userModel);
                    await _userManager.RemoveFromRolesAsync(userModel, await _userManager.GetRolesAsync(userModel));

                    // Step 2. 갱신된 Role 추가
                    if (user.Roles.Count > 0)
                    {
                        var updateRoles = from r in user.Roles
                                          where r.Checked == true
                                          select r.Value;
                        if (updateRoles.Contains("Admin"))
                        {
                            if (!IsAdminUser()) 
                            {
                                await _userManager.AddToRolesAsync(userModel, originalRoles);
                                return RedirectToAction("AccessDenied", "Account");
                            }
                        }

                        roleUpdateResult = await _userManager.AddToRolesAsync(userModel, updateRoles);

                        // Step 3. 갱신된 Role 추가 시 에러가 발생하면 기존에 삭제한 role 복원
                        if (!roleUpdateResult.Succeeded)
                        {
                            await _userManager.AddToRolesAsync(userModel, originalRoles);
                            AddErrors(roleUpdateResult);
                        }
                        else 
                        {
                            return RedirectToAction(nameof(Index));
                        }
                    }
                }
                catch (DbUpdateConcurrencyException e)
                {
                    if (!await VodafoneAdminUserExists(user.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        AddErrors(e);
                    }
                }
                catch (InvalidOperationException ex)
                {
                    AddErrors(ex);
                }
            }
            return View(user);
        }
        #endregion

        #region Delete
        public async Task<IActionResult> Delete(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return NotFound();
            }
            var user = await _userManager.Users.SingleOrDefaultAsync(u => u.Id == id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var user = await _userManager.Users.SingleOrDefaultAsync(u => u.Id == id);
            var result = await _userManager.DeleteAsync(user);
            if(result.Succeeded)
            {
                return RedirectToAction(nameof(Index));
            }
            return View(user);
        }
        #endregion

        #region Create
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AdminUserCreateViewModel createUser)
        {
            if(ModelState.IsValid)
            {
                var user = new ApplicationUser { UserName = createUser.Email, Email = createUser.Email };
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);
                    string callbackUrl = string.Empty;

                    if(_hostingEnv.EnvironmentName == "Development")
                    {
                        callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);
                    }
                    else
                    {
                        callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, "https");
                    }
                    await _emailSender.SendEmailAsync(user.Email, "Invite V-SOS Admin Management Site",
                   $"You have been invited to the Vodafone elderly admin management site. Please set up your password by clicking here: <a href='{callbackUrl}'>link</a>");

                    return RedirectToAction(nameof(Index));
                }
                AddErrors(result);
            }
            return View(createUser);
        }
        #endregion
    }
}