﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using VFSM.DAL;
using VFSM.DAL.Models;

namespace VFSM.API.Controllers.API
{
    public class BaseController : Controller
    {
        private readonly VodafoneContext _vodaContext;
        private readonly ApplicationDbContext _context;
        private readonly IUnitOfWork _unitOfWork;
        protected UserManager<ApplicationUser> _userManager;
        private readonly IMemoryCache _cache;

        public BaseController(VodafoneContext vodaContext,
           ApplicationDbContext context,
           IUnitOfWork unitOfWork,
           UserManager<ApplicationUser> userManager,
           IMemoryCache cache)
        {
            _vodaContext = vodaContext;
            _context = context;
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _cache = cache;
        }

        public IMemoryCache _memCache => (IMemoryCache)_cache;
        public VodafoneContext _vfDbContext => (VodafoneContext)_vodaContext;
        public ApplicationDbContext _appDbContext => (ApplicationDbContext)_context;

        public IUnitOfWork _vodaUnitOfWork => (IUnitOfWork)_unitOfWork;

        public UserManager<ApplicationUser> _vodaUserManager => (UserManager<ApplicationUser>)_userManager;

        #region Exists methods
        protected bool VodafoneModelExists(string id)
        {
            return _vfDbContext.VodafoneModels.Any(e => e.ModelId == id);
        }

        protected bool VodafoneDeviceExists(string id)
        {
            return _vfDbContext.VodafoneDevices.Any(e => e.DeviceId == id);
        }

        protected bool VodafoneOTAExists(string modelId, string category, string version)
        {
            return _vfDbContext.VodafoneOtas.Any(o => o.ModelId == modelId && o.Category == category && o.Version == version);
        }

        protected bool VodafoneOTAMappingVersionExists(string mappingVersion)
        {
            return _vfDbContext.VodafoneOtas.Any(o => o.MappingVersion == mappingVersion);
        }

        protected bool VodafoneAdminUserExists(string id)
        {
            return _userManager.Users.Any(u => u.Id == id);
        }
        #endregion

        /// <summary>
        /// Get object user current login by info token
        /// Check by username or email 
        /// </summary>
        /// <returns></returns>
        [NonAction]
        public async Task<ApplicationUser> GetUserAsync()
        {
            // Get info from token
            var user = await _vodaUserManager.FindByNameAsync(_appDbContext.CurrentUserName);

            if (user != null)
                return user;
            return null;
        }

        public void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }
        public T CacheAction<T>(Expression<Func<T>> action, [CallerMemberName] string memberName = "") where T : class
        {
            MethodCallExpression body = (MethodCallExpression)action.Body;

            ICollection<object> parameters = new List<object>();

            foreach (MemberExpression member in body.Arguments)
            {
                var field = (FieldInfo)member.Member;
                var param = field.GetValue(((ConstantExpression)member.Expression).Value);
                if (!field.GetType().IsPrimitive)
                {
                    var getPro = new List<string>();
                    foreach (var prop in param.GetType().GetProperties())
                    {
                        getPro.Add(prop.Name.ToString());
                    }
                    foreach (var item in getPro)
                    {
                        // get value of object 
                        var value = param.GetType().GetProperty(item).GetValue(param, null);
                        if (value != null)
                        {
                            parameters.Add(value);
                        }
                    }
                }
                else
                {
                    parameters.Add(param);
                }
            }

            StringBuilder builder = new StringBuilder(100);
            builder.Append(GetType().FullName);
            builder.Append(".");
            builder.Append(memberName);

            parameters.ToList().ForEach(x =>
            {
                builder.Append("_");
                builder.Append(x);
            });

            string cacheKey = builder.ToString();

            T retrieve = _memCache.Get<T>(cacheKey);

            if (retrieve == null)
            {
                retrieve = action.Compile().Invoke();
                MemoryCacheEntryOptions cacheExpirationOptions = new MemoryCacheEntryOptions();
                cacheExpirationOptions.AbsoluteExpiration = DateTime.Now.AddMinutes(60);
                cacheExpirationOptions.Priority = CacheItemPriority.Normal;
                // _memCache.Set<string>("TestKey", DateTime.Now.ToString(), cacheExpirationOptions); 
                _memCache.Set(cacheKey, retrieve, cacheExpirationOptions);
            }

            return retrieve;
        }
    }
}