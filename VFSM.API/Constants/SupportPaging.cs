
namespace VFSM.API
{
    public static class PagingSupport
    {
        // CreatedDate
        public const bool GetTotal = true;
        public const bool DontGetTotal = false;
    }
}