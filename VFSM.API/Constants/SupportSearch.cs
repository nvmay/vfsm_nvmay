
namespace VFSM.API
{
    public static class SortUser
    {
        // CreatedDate
        public const string CreatedDate = "CreatedDate";

        // UserId
        public const string UserId = "UserId";

        // NickName
        public const string NickName = "NickName";

        // MobileType
        public const string MobileType = "MobileType";

        // Imei
        public const string Imei = "Imei";
    }
    public static class SortModel
    {
        public const string CreatedDate = "CreatedDate";
        
        public const string ModelId = "ModelId";
        
        public const string Name = "Name";
        
        public const string ModifiedDate = "ModifiedDate";
    }
    public static class SortActivity
    {
        public const string ActivatedDate = "ACTIVATEDDATE";
        
        public const string Imei = "IMEI";
    }
    public static class TypeActivity
    {
        public const string History = "HISTORY";
        public const string SOS = "SOS";
        public const string FALL = "FALL";
        public const string Battery = "BATTERY";
    }
    public static class SortAccount
    {
        public const string UserName = "USER_NAME";
        public const string Email = "EMAIL";
    }
    public static class SortRole
    {
        public const string Name = "NAME";
    }
}