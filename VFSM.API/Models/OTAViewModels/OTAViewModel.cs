using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace VFSM.API.Models
{
    public class OTAViewModel
    {
        [Required]
        [Display(Name = "Model ID")]
        public string ModelId { get; set; }

        [Required]
        [Display(Name = "Version")]
        [RegularExpression(@"^\d+\.\d+\.\d+\.\d+")]
        public string Version { get; set; }

        [Required]
        [Display(Name = "Category")]
        public string Category { get; set; }
        
        [Display(Name = "Active")]
        public bool IsActive { get; set; }
        
        [Required]
        [Display(Name = "File")]
        public IFormFile File { get; set; }
    }
    public class OTAViewNotFileModel
    {
        [Required]
        public string ModelId { get; set; }

        [Required]
        [RegularExpression(@"^\d+\.\d+\.\d+\.\d+")]
        public string Version { get; set; }

        [Required]
        public string Category { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
    public class OTACreateViewModel
    {
        [Required]
        public string ModelId { get; set; }
        [Required]
        [RegularExpression(@"^\d+\.\d+\.\d+\.\d+")]
        public string Version { get; set; }
        [Required]
        public string Category { get; set; }
        public bool IsActive { get; set; }
        public FileViewModel File { get; set; }
        public string MappingVersion { get; set; }
    }
    public class OTAUpdateViewModel
    {
        [Required]
        public string ModelId { get; set; }
        [Required]
        public string Version { get; set; }
        [Required]
        public string Category { get; set; }
        public bool IsActive { get; set; }
        public FileViewModel File { get; set; }
        public string MappingVersion { get; set; }
    }
    public class OTADeleteViewModel
    {
        [Required]
        public string ModelId { get; set; }

        [Required]
        public string Version { get; set; }

        [Required]
        public string Category { get; set; }
    }
    public class OTADeleteResponseViewModel
    {
        public List<OTADeleteViewModel> ListSuccess { get; set; }
        public List<OTADeleteViewModel> ListFail { get; set; }
    }
}