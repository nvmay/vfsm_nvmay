﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VinaHR.DAL;

namespace VFSM.API.Models
{
    public class PagingViewModel : IPagingParams
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
    }
}
