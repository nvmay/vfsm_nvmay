﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using VinaHR.DAL;

namespace VFSM.API.Models
{
    public class ModelViewModel
    {
        public string ModelId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class ModelSearchViewModel : SortViewModels
    {
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime FromDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime ToDate { get; set; }
        public string ModelId { get; set; }
        public string Name { get; set; }
    }
    public class ModelDetailViewModel
    {
        public string ModelId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? CreatedDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? ModifiedDate { get; set; }
        public bool DelFlag { get; set; }
        public string VersionMC60 { get; set; }
        public string VersionFirmware { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd HH:mm:ss}")]
        public DateTime? LastUpdateDate { get; set; }
        
    }
}
