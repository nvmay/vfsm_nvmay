
namespace VFSM.API.Models
{
    public class ResponseViewModel
    {
        private string status = "Success";
        public string Status { get { return status; } set { status = value; } }
        public dynamic Data { get; set; }
    }
}