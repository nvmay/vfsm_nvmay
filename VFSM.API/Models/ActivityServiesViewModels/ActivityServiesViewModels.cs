﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VinaHR.DAL;

namespace VFSM.API.Models
{
    public class ActivityServiesExportViewModels
    {
        public int ActSeriesSeq { get; set; }
        public string DeviceId { get; set; }
        public string ActId { get; set; }
        public string EventType { get; set; }
        public string Message { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
