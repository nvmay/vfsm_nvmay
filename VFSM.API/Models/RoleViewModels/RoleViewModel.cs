using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VFSM.API.Models.RoleViewModels
{
    public class RoleViewModel
    {
        [Display(Name = "Role ID")]
        public string Id { get; set; }

        [Required]
        [StringLength(256, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        [DataType(DataType.Text)]
        [Display(Name = "Role Name")]
        public string Name { get; set; }

        [Display(Name = "User Count")]
        public int UserCount { get; set; }
    }

    public class RolesViewMode
    {
        public string Id { get; set; }

        [Required]
        [StringLength(256, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 2)]
        public string Name { get; set; }
        public string Description { get; set; }

        [Required]
        public IEnumerable<PageViewModel> Pages { get; set; }
    }

    public class PageViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        public int Order { get; set; }
        public string Parent_ID { get; set; }
        public string OperationID { get; set; }

        //public IEnumerable<OperationViewModel> Operations { get; set; }
    }

    public class OperationViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
    public class RoleSearchViewModel : SortViewModels
    {
        public string Name { get; set; }
    }
}
