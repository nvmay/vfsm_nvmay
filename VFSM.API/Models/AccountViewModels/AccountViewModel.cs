﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace VFSM.API.Models.AccountViewModels
{
    public class AccountSearchViewModel : SortViewModels
    {
        public string Email { get; set; }
    }
    public class AccountDetailViewModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public string RoleId { get; set; }
        public string Role { get; set; }
    }
}
