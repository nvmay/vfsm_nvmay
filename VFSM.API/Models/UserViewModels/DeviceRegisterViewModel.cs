using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace VFSM.API.Models.UserViewModels
{
    public class DeviceRegisterViewModel
    {
        [Required]
        public string UserId { get; set; }
        public string DeviceUid { get; set; }
        public string DeviceName { get; set; }
        public string Role { get; set; }
    }
    public class DeviceResponseViewModel
    {
        [Required]
        public string UserId { get; set; }
        public string Role { get; set; }
        public string DeviceId { get; set; }
        public string Status { get; set; }
        public DateTime? ActivatedDate { get; set; }
        public string ActivityId { get; set; }
        public bool Activated { get; set; }
        public int? Battery { get; set; }
        public string BeaconMac { get; set; }
        public string DeviceName { get; set; }
        public string DeviceVersion { get; set; }
        public string Imei { get; set; }
        public double Lat { get; set; }
        public double Lng { get; set; }
        public string LatBeacon { get; set; }
        public string LngBeacon { get; set; }
        public string ModelId { get; set; }
        public string Version { get; set; }
    }
}