using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace VFSM.API.Models.UserViewModels
{
    public class DeviceNameModifyViewModel
    {
        [Required]
        [Display(Name = "Device Id")]
        public string DeviceId { get; set; }

        [Required]
        [Display(Name = "Imei")]
        public string DeviceUid { get; set; }
        
        [Display(Name = "Input New Device Name")]
        public string Name { get; set; }

        [Display(Name = "User Id")]
        public string UserId { get; set; }

        [Display(Name = "Page")]
        public int Page { get; set; }
    }
}