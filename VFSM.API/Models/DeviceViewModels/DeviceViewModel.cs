﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VinaHR.DAL;

namespace VFSM.API.Models
{
    public class DeviceViewModel
    {
        public string DeviceId { get; set; }
        public string Name { get; set; }
    }
    public class DeviceActivityViewModel
    {
        public string DeviceId { get; set; }
        public string Imei { get; set; }
        public int? Battery { get; set; }
        public bool Activated { get; set; }
        public DateTime ActivatedDate { get; set; }
    }
    public class SearchDeviceViewModel
    {
        public string DeviceId { get; set; }
        public string Name { get; set; }
        public string DeviceUid { get; set; }
    }
    public class DeviceExportListViewModel
    {
        public string DeviceId { get; set; }
        public string Name { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CustomerId { get; set; }
        public string Imei { get; set; }
        public string Version { get; set; }
        public string Role { get; set; }
        public string Status { get; set; }
        
    }
    public class DeviceResponseModel
    {
        public string DeviceId { get; set; }
        public string ActivityId { get; set; }
        public string DeviceVersion { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public string BeaconMac { get; set; }
        public int? Battery { get; set; }
        public DateTime? ActivatedDate { get; set; }
        public string Status { get; set; }
        public string Imei { get; set; }
        public string DeviceName { get; set; }
        public string Version { get; set; }
        public double? LatBeacon { get; set; }
        public double? LngBeacon { get; set; }
    }
}
