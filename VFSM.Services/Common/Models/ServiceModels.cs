using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace VFSM.Services
{ 
    public class ExportFileViewModel
    {
        [Required]
        public IEnumerable<dynamic> Data { get; set; }
        public string TemplateFileName { get; set; }
        public int HeaderRow { get; set; }
        public int DataStartRow { get; set; }
    }
    //public class DeviceResponseViewModel
    //{
    //    [Required]
    //    public string UserId { get; set; }
    //    public string DeviceId { get; set; }
    //    public string DeviceName { get; set; }
    //    public string Role { get; set; }
    //}
}