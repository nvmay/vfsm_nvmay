﻿using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using OfficeOpenXml;

namespace VFSM.Services
{
    public class ExcelFileService : IExcelFileService
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfiguration _config;
        public ExcelFileService(IHostingEnvironment env, IConfiguration config)
        {
            _env = env;
            _config = config;
        }

        public dynamic FillDataIntoTemplate(ExportFileViewModel dataModel)
        {
            var listData = dataModel.Data;
            var fileName = dataModel.TemplateFileName;
            var headerRow = dataModel.HeaderRow;
            var dataStartRow = dataModel.DataStartRow;
            var toDay = DateTime.Now;
            var day = toDay.Day;
            var year = toDay.Year;
            var newFile = _env.WebRootPath + @"\Templates\FileTemplate\" + fileName + day + year + ".xlsx";
            try
            {
                //Opening an existing Excel file
                FileInfo fileTemp = new FileInfo(_env.WebRootPath + @"\Templates\FileTemplate\" + fileName + ".xlsx");
                FileInfo fi = fileTemp.CopyTo(newFile);
                using (ExcelPackage excelPackage = new ExcelPackage(fi, fileTemp))
                {
                    //Get a WorkSheet by index. Note that EPPlus indexes are base 1, not base 0!
                    //ExcelWorksheet firstWorksheet = excelPackage.Workbook.Worksheets[0];

                    //Get a WorkSheet by name. If the worksheet doesn't exist, throw an exeption
                    ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[0];                    

                    //select column
                    List<string> listColumn = new List<string>();
                    var endColumn = worksheet.Dimension.End.Column;
                    foreach (var cell in worksheet.Cells[1, 1, 1, endColumn])
                    {
                        string columnName = cell.Text.Replace(" ", "");
                        listColumn.Add(columnName);
                    }
                    worksheet.DeleteRow(1);
                    foreach (var item in listData)
                    {
                        worksheet.Cells[dataStartRow, 1, dataStartRow, endColumn].Copy(worksheet.Cells[dataStartRow + 1, 1, dataStartRow + 1, endColumn]);
                        for (var i = 0; i < listColumn.Count; i++)
                        {
                            if(item.GetType().GetProperty(listColumn[i]).GetValue(item, null) == null || item.GetType().GetProperty(listColumn[i]).GetValue(item, null).ToString().Contains("01/01/0001"))
                                worksheet.Cells[dataStartRow, i + 1].Value = string.Empty;
                            else
                                worksheet.Cells[dataStartRow, i+1].Value = item.GetType().GetProperty(listColumn[i]).GetValue(item, null).ToString();
                        }
                        dataStartRow++;
                    }
                    worksheet.DeleteRow(dataStartRow);
                    //Save your file
                    excelPackage.Save();
                    var fileReturn = excelPackage.GetAsByteArray();
                    fi.Delete();
                    return fileReturn; 
                }

            }
            catch (Exception ex)
            {
                FileInfo fiRemove = new FileInfo(newFile);
                fiRemove.Delete();
                throw ex;
            }
        }
        public dynamic FillDataIntoTemplateMultiSheet(List<ExportFileViewModel> listDataModel, string fileName)
        {
            var toDay = DateTime.Now;
            var day = toDay.Day;
            var year = toDay.Year;
            var newFile = _env.WebRootPath + @"\Templates\FileTemplate\" + fileName + day + year + ".xlsx";
            try
            {
                //Opening an existing Excel file
                FileInfo fileTemp = new FileInfo(_env.WebRootPath + @"\Templates\FileTemplate\" + fileName + ".xlsx");
                FileInfo fi = fileTemp.CopyTo(newFile);
                using (ExcelPackage excelPackage = new ExcelPackage(fi, fileTemp))
                {
                    for (int i = 0; i < listDataModel.Count; i++)
                    {
                        var listData = listDataModel[i].Data;
                        var headerRow = listDataModel[i].HeaderRow;
                        var dataStartRow = listDataModel[i].DataStartRow;
                        ExcelWorksheet worksheet = excelPackage.Workbook.Worksheets[i]; 
                        //select column
                        List<string> listColumn = new List<string>();
                        var endColumn = worksheet.Dimension.End.Column;
                        // [int FromRow, int FromCol, int ToRow, int ToCol]
                        foreach (var cell in worksheet.Cells[1, 1, 1, endColumn])
                        {
                            string columnName = cell.Text.Replace(" ", "");
                            listColumn.Add(columnName);
                        }
                        worksheet.DeleteRow(1);
                        foreach (var item in listData)
                        {
                            worksheet.Cells[dataStartRow, 1, dataStartRow, endColumn].Copy(worksheet.Cells[dataStartRow + 1, 1, dataStartRow + 1, endColumn]);
                            for (var c = 0; c < listColumn.Count; c++)
                            {
                                if(item.GetType().GetProperty(listColumn[c]).GetValue(item, null) == null || item.GetType().GetProperty(listColumn[c]).GetValue(item, null).ToString().Contains("01/01/0001"))
                                    worksheet.Cells[dataStartRow, c + 1].Value = string.Empty;
                                else
                                    worksheet.Cells[dataStartRow, c + 1].Value = item.GetType().GetProperty(listColumn[c]).GetValue(item, null).ToString();
                            }
                            dataStartRow++;
                        }
                        //worksheet.DeleteRow(dataStartRow); 
                    }
                    //Save your file
                    //excelPackage.Save();
                    var fileReturn = excelPackage.GetAsByteArray();
                    fi.Delete();
                    return fileReturn; 
                }

            }
            catch (Exception ex)
            {
                FileInfo fiRemove = new FileInfo(newFile);
                fiRemove.Delete();
                throw ex;
            }
        }
    }
}
