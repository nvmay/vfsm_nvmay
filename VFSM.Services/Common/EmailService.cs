﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using Amazon.SimpleEmail;
using Amazon.SimpleEmail.Model;
using Amazon;
using VFSM.DAL.Config;
using Microsoft.Extensions.Configuration;
using Amazon.Runtime;

namespace VFSM.Services
{
    // This class is used by the application to send email for account confirmation and password reset.
    // For more details see https://go.microsoft.com/fwlink/?LinkID=532713
    public class EmailService : IEmailService
    {
        private readonly AWSConfig _awsConfig;
        private IConfiguration _configuration;
        private readonly AmazonSimpleEmailServiceClient _sesClient;

        public EmailService(
            IOptions<AWSConfig> awsConfig,
            IConfiguration configuration)
        {
            _awsConfig = awsConfig.Value;
            _configuration = configuration;
            var credentials = new BasicAWSCredentials(_awsConfig.AWS_ACCESSS_KEY_ID, _awsConfig.AWS_SECRET_ACCESS_KEY);
            _sesClient = new AmazonSimpleEmailServiceClient(credentials, RegionEndpoint.EUWest1);
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            // using (var client = new AmazonSimpleEmailServiceClient(RegionEndpoint.EUWest1))
            // {
                // var sendRequest = new SendEmailRequest
                // {
                //     Source = _configuration["AWSConfig:AWS_EMAIL_SENDER"],
                //     Destination = new Destination
                //     {
                //         ToAddresses =
                //         new List<string> { email }
                //     },
                //     Message = new Message
                //     {
                //         Subject = new Content(subject),
                //         Body = new Body
                //         {
                //             Html = new Content
                //             {
                //                 Charset = "UTF-8",
                //                 Data = message
                //             }
                //         }
                //     },
                                        
                // };
                // return client.SendEmailAsync(sendRequest);
                // string senderAddress = _configuration["AWSConfig:AWS_EMAIL_SENDER"];
                var receiverAddress = new List<string> { "hyrin65@gmail.com" };
                if(Convert.ToBoolean(_configuration["IsModeProduct"]))
                {
                    receiverAddress = new List<string> { email };
                }
                var sendRequest = new SendEmailRequest
                {
                    Source = _configuration["AWSConfig:AWS_EMAIL_SENDER"],
                    Destination = new Destination
                    {
                        ToAddresses = receiverAddress
                    },
                    Message = new Message
                    {
                        Subject = new Content(subject),
                        Body = new Body
                        {
                            Html = new Content
                            {
                                Charset = "UTF-8",
                                Data = message
                            }
                        }
                    },
                                        
                };
                try
                {
                    // Sending email using Amazon SES... 
                    return _sesClient.SendEmailAsync(sendRequest);
                    // The email was sent successfully.
                }
                catch (Exception ex)
                {
                    //("The email was not sent." + ex.Message);
                    throw ex;
                }
            // }
        }
    }
}
